# How to get our Plug-in onto the Eclipse Marketplace(MP)

## Legal
New listings in the MP, such as our plug-in, always need approval by the MP administrator.
"We will try to approve all items within 24hrs of submission. If you have any questions, Eddie (not our Eddie) is the person that does most of the approval, so feel free to contact him at marketplace-support@eclipse.org"

## From Project to finished Plug-in
This part has a lot of overlap with the deploying and testing of the plug-in, and because there are separate tasks for that it is not researched here.

## From finished Plug-in to Listing
1. Create an account on the MP and login.
2. 'Once you are logged into Eclipse Marketplace you will be able to add new content to the site using the 'Add Content' Link in the top nav bar.''
3. From the two types of listings on the MP (Solutions and Training&Consulting), our plug-in is a Solution.
4. 'Once you've submitted your listings it will be placed into the Moderation Queue and should appear on Marketplace in the next 24 business hours.'

Making a separate gmail-account for CLIDE or maybe Franc's e-mail to create an account on the MP is still to be decided. Maybe Franc already has an MP account, which we could then use. This will be discussed by the proxy product owner and the product owner.

## "Add Content"-option
Investigating how to actually submit a project, I created my own account on Eclipse.org. When clicking on 'Add Content', and subsequently on 'Add Solution', I reached a page in which I am prompted to fill in information in boxes. Following is a list of the required information, which might be handy to think about already (suggestions for answers are found below the requirements):
* Solution Name   
(CLIDE Plug-in)
* Eclipse Foundation Member Organization  
(No)
* Solution Description  
(A plug-in for Eclipse serving as an IDE for the functional programming language Clean (currently version 3.0). It was created as a class-project by a group of 7 students studying at the Radboud University.)
* Categories  
(IDE)
* Markets   
(RCP Applications)
* Update Site url   
(Required for this is that we create an update site for our installed plug-in. This is done through the IDE, in which you first create a Feature Project to link with our Plug-in Project. After doing this we have to create an Update Site Project to which we then link the Feature Project we just created. The full tutorial: https://wiki.eclipse.org/FAQ_How_do_I_create_an_update_site_(site.xml)%3F )
* Supported Eclipse Release(s)  
(Eclipse IDE for Java Developers
Version: 2018-09 (4.9.0)
Build id: 20180917-1800)
* Support Platform(s)   
(Windows, Mac, & Linux)
* Minimum Java version  
(Java version: 1.8.0_192)
* Feature ID's  
(Information on how to specify the Feature ID's can be found in the last section of this document)

## From Listing to integration into built-in MP client

'When reading the following, please keep in mind the distinction between the “Eclipse Marketplace” (a marketplace catalogue hosted by the Eclipse Foundation), a “Marketplace” (a marketplace catalogue hosted by a third party) and the “MPC” (a client-side tool installed into an Eclipse workspace).''

The MPC has two interface modes: 1) MPC Wizard and 2) MPC Embedded Browser. The MPC Wizard is the interface presented to the end user when it is started from Eclipse. Solutions listed in the MPC Wizard will only be those that are
1. licensed under an open source license or
2. associated with an Eclipse Foundation member company (excluding Associate Members).

The initial feature list in the MPC Wizard will only include solutions from Foundation member companies. Search results and Category listings will include open source license solutions.

So since our license is open-source, it is possible for CLIDE to be on the MPC Wizard. I suggest we aim to do this since it will probably be hard enough for them second-years to get it working.

###### Requirements:
* Your product must be able to install into an existing Eclipse installation. Unfortunately, the Marketplace client can't support products that require a new installation of Eclipse.
* Your product must be able to install into an existing Eclipse installation. Unfortunately, the Marketplace client can't support products that require a new installation of Eclipse.
  1. You product needs to be downloadable from an Eclipse p2 update site. Your listing on Marketplace requires the url to that Update Site.
  2. You need to define the default installation of your product from this update site. A default installation consists of 1 or more installable features, which are identified by Feature IDs. You will need to specify these default Feature IDs in your Marketplace listing.

(Logically after this we test whether it worked using a current version of the MP client.)


In short, it is hardly (if at all) different from just listing on the MP, but still handy to keep as a checklist just to be sure.


###### How to specify the Feature IDs
Feature ID's are used by the Eclipse Marketplace Client (MPC) to determine which features to install for your listing. These ID's are key in making sure that the MPC is able to install your products successfully.

You can find these IDs by opening the site.xml file at the root of your Eclipse Update site. Inside this file you will find a list of <feature> tags for each of the features present on your Update Site. Please provide the the feature id string for each feature that should be installed by default for your product.

(For an example of exactly what this means: https://marketplace.eclipse.org/quickstart)
