# Using CPM on Windows

- Compiling a clean program is possible using cpm, the "Clean Project Manager"
- To compile a project, the cpm expects a project file that contains information on all the files and preferences for the project
- On windows we encountered issues with running the cpm from cmd or powershell, the correct usage seems to be:

1) Navigate to the folder of your project (not the cpm!) using cd: `PS> cd "C:\Users\Edwin\Clean 3.0"`
2) Call the cpm with the project file as an argument, i.e. give the absolute path to the cpm executable on your system and then the name of the .prj file as an argument. 
	- If you use cmd: `> "C:\Users\Edwin\Clean 3.0\cpm.exe" HelloWorld.prj`
	- If you use PowerShell, you don't use quotes. But then you need to escape spaces in your path with backtics: C:\Users\Edwin` Wenink\Clean` 3.0\cpm.exe
	- Alternatively, if the folder only contains one project or one wishes to build all projects in a directory it is sufficient to run `cpm make` in that folder (again giving the absolute path to the cpm).
