# Research on Feasability of Writing a Plugin for Atom

## Research

- Atom supports IDE functionality
    - atom-ide package available for installation
    - packages with support for various languages exist haskell, python, java
    - tested briefly for Java, does not feel like a "proper" IDE
    - features code navigation (look up definitions of functions etc)
    - there is an existing clean language plugin for atom that handles syntax highlighting
    - atom-runner package allows for code execution in the editor, no clean support but extendable according to git hub page (does not handle user interaction?)
    
- native support for git

- compatible with linux, windows and macos

- built on electron
    - javascript
    - css
    - html
    - Node.js

- all source code available on github (open source)
- tutorials available on github
- extensive documentation of the api available


## Summary

*+*

- open source
- existing resources that can be extended
- developers seem to focus on making package development easy


*-*

- packages implementing IDE functionality don't immediately feel intuitive
- "just" and editor
