# Color Coding Console Output

Color can be coded in multiple ways, there are certain characters available that will result in pre-defined colors, and others to define your own[to be implemented].
for now the CHaracters are in the range 
05f5 and 05FF [these are free spots from unicode after the \" series].

|   |   |   |   |   |
|---|---|---|---|---|
| character | color |
| 0x05f5| <span style="color:black"> black|
| 0x05f6|  <span style="color:red"> red </span>|
| 0x05f7| <span style="color:green">green|
| 0x05f8| <span style="color:yellow">yellow|
| 0x05f8| <span style="color:blue">blue|

in Java escaping is done with "\uCODE"

> e.g. "this is \u05f7 CLIDE \0x05f5 !!" \
should print to the console:
> <span style="color:black"> this is <span style="color:green"> CLIDE <span style="color:black"> !!
