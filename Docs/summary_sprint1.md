# Research Report - Sprint 

In order to decide on a platform to build the CLIDE ide on we spent some time on researching relevant options. In order to come to an informed decision we researched both frameworks to build the application from scratch as well as the possibility to write a plugin for an existing editor / ide. Our research was presented to the product owner.


## Research Conducted:

1. Research into Plugins:
    - Eclipse
    - Visual Studio
    - Netbeans
    - Atom
2. Research into developing a Standalone Application


## Decision and Summary of Research

All research summaries are included in the corresponding tasks in Jira for reference. The main take away from our research is that eclipse seems to be the most suitable 'basis' upon which to build a plugin. Reasons for this include the availability of a large ecosystem of plugins that we can use for reference and the fact that writing plugins for Eclipse is supported well and even has an IDE dedicated to plugin development. Eclipse is available for all major OS's. Further, the product owner advocated for Java and the Eclipse IDE as a basis specifically.

An alternative would be be to use the text editor Atom as a basis since it is highly extensible and plugin development is well supported. In addition there already are packages that offer syntax highlighting for Clean that could be reused.
As a third option we could develop our own IDE from scratch using the electron framework. This will allow for the greatest flexibility and offers an opportunity to design an IDE exactly the way we want to. This would be a great learning opportunity but will most likely also require more time before we can deliver a usable product.

For now we choose to build CLIDE as a plugin for Eclipse but keep Atom and and Electron app as a second and third option in case Eclipse should proove unsuitable for any reason. This also means that the next sprint will likely include several additional spikes that will be used to gain more in depth insight into developing Eclipse Plugins. Further, in a meeting with the product owner Edwin already received some information on features that have the highest priority for the client. In the upcoming sprint we could already focus on gathering info on these features specifically.


## Deliverables

- Documents summarizing research mentioned above
- Document summarizing the first meeting with the client
- Document summarizing the SCRUM process and the roles involved
