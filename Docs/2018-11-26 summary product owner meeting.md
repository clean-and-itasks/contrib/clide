# Summary Product Owner Meeting | 2018-11-26

## Introduction 

I updated Franc Grootjen on the progress made last week. If we do not encounter unsuspected major issues, we likely are able to compile and run clean files from within the IDE after this week. I also mentioned that although our efforts show results, we do struggle with the Scrum process quite a bit. In particular we have the issue that we struggle to provide each team member with the desired amount of hours per week. Last sprint we made the mistake that we did not split up large tasks into subtasks properly, making it harder to divide and parallelize work. Apart from hearing Franc's wishes for the product, a main focus of this meeting was therefore to brainstorm on feasible tasks and goals that are preferably easy to parallelize.   

Before the meeting I took some input from the team, that I also passed through to Franc.
I will summarize the points discussed in order:

## Point by point summary 

- To ensure that our application will be cross-platform, I asked if we can have access to the Mac in the "TK". Franc will create an account for us. I will follow up on this when I see Franc next time. N.B. Franc however mentioned that if in the worst case not everything runs on Mac, that is no reason for panic. I however do think that compatibility across platforms is something we should really try to ensure, as it was a main intention behind this project.

- A possible goal we proposed for next sprint is having syntax highlighting. I brought this up again because it would be a nice parallel task. In our first conversation, Franc however mentioned that syntax highlighting has a *very* low priority for him. Syntax highlighting requires a parser, and building a parser is a hard problem. Building a parser for a *functional* programming language is even harder. However, this assumes that we have to build a parser from scratch. The main reason for the low priority of syntax highlighting is thus clearly its difficulty. If we however find a way to avoid building a parser from scratch, syntax highlighting is definitely something that is cool for the project, and really enhances the "look and feel" of using CLIDE. Some options:
	- Re-use regex for parsing, given that this problem was (more or less?) successfully solved by the vim-plugin we found in the first research sprint.
	- Use a tool like ANTRL to build a parser. This assumes that the Clean parser is 1) built on a grammar and 2) that this grammar is available to us. Franc was sceptical about this and suspect the parser is built up in various layers, because this is the case for other functional programming languages such as SCALA.
	- This generates some possible research tasks to investigate the feasibility of this task for next sprint: 1) check the quality of regex in vim, 2) investigate whether a grammar of Clean is available. 
- Since after this sprint we are able to generate .prj project files, we can spend time in the next sprint to create a nice project view. 
- We can develop smaller handy features, such as the automatic creation of .dcl files from .icl files. Franc assumed this would be quite some work, but I think we can solve this by using the functionality of the Clean project manager cpm.
- Another such functionality is the ability to click on a function name where it is called, and then jump to the corresponding function definition. This may be a hard problem, e.g. taking into account that a lot of functions are imported from other modules.
	- A question to research: does this problem also requires a parser? Then similar issues to syntax highlighting issues apply to this task.
	- Is the "shadowing" of function names an issue in Clean? Shadowing: when the same function name in an inner scope is used as in the outer scope.
- I asked Franc about wishes for the layout / UI. He had no other preferences other than the standard layout, with a project view on the left, editor perspective in the center, and console / output perspective at the bottom.
- Another possible parallel task is research into how to deploy the plugin once it is done. It could be worthwhile to already look into it, to prevent delay when the product is done and we want to ship it, for example to the Eclipse plugin market. This could be split up in subtasks, along the following lines: 1) how to deploy the plugin itself independent from the Clean language? 2) How to deal with dependency of the plugin on a particular Clean version? E.g. 3.0 vs 2.0? And future versions? Do you ship it with the plugin? 
- Other small functionalities could be some rapports on statistics of your Clean program. E.g. what is the current stack size? Or anything else that might be useful.
- Following that up: integrate functionality for easily adjusting the settings mentioned by those statistics, e.g. the stack size.
	- I suggested we can perhaps efficiently use the cpm tool for this. E.g. run 
	\-- verbose and distill relevant info. Or otherwise distill info from the project file directly.
- Insofar as this is still applicable for next sprint, we can spend time on the good integration of functionality so far.
- In general, Franc recommended that we should check the CleanIDE for handy functionalities in case we are looking for more functionalities to work on. He trusts our judgement on what is useful and what not.
- I asked about what a good time is to involve Peter Achten into the project. Franc said that it is completely up to us, but he did make the strong recommendation that when we involve Peter Achten we should have something to show to him already. If that is the case, the involvement of Peter Achten is useful to see what functions are most needed and desired by the people actually using Clean.

## Conclusion

The requirements for our project did not change during this meeting. 

The project is on track.

We are by now relatively free to choose the functionality we want to implement next, because Franc does not have very strong preferences. It might soon be interesting to involve Peter Achten to see what his wishes are.
