# Classes that need refactoring, sorted by folder (04/01/2019)
## dclGenerator
###### DclGenerationHandler.java
  In many @override functions are empty, all automatically generated TODO comments could be removed maybe.
###### DclGenerator.java
  Perfect.

## editors
###### CleanColorConstants.java
  Perfect.
###### CleanConfiguration.java
  * 2 Methods that are commented out completely.
  * No javadoc present.
  * In IPresentationReconciler a few lines are commented out.

###### CleanPartitionScanner.java
  Many comments still present.
###### CleanScanner.java
  A few fields commented out.
###### CleanWhiteSpaceDetector.java
  One comment containing a question.
###### ColorManager.java
  Perfect
###### DCLEditor.java
  One line of code commented out.
###### FunctionRule.java
  An earlier version of the code at the top is commented out at the bottom of the file.
###### ICLEditor.java
  One line of code commented out.

## externalcommands
###### ExternalCommandService.java
  Perfect.

## feedback
###### ColorConstants.java
  Perfect.
###### ConsoleInterfacer.java
  small typo in javadoc of 'getColor'.

## launchconfigurations
###### CleanLaunchConstants.java
  Perfect.
###### LaunchConfigurationTabGroupClean.java
  Perfect.
###### MainTab.java
  * In method 'initializeFrom' a decision about the 'I think the following two lines can be deleted' comment should be made.
  * In method 'canSave' a comment after the return is present containing some code that is not used.
  * In method 'createControl' a comment with a sysout is present.
  * In method 'getName' TODO comment can be removed.

###### ProjectContentProvider.java
  Perfect.
###### SourceFileContentProvider.java
  Perfect.

## launchdelegates
###### There is a separate task for the refactoring the CleanLaunchDelegate.

## parser
###### CleanCompileTimeError.java
  Perhaps the information at the top has to be removed.
###### CleanError.java
  Perfect.
###### CleanMessage.java
  Perfect.
###### CleanOutput.java
  Perfect.
###### CleanRuntimeError.java
  * Large block of comments containing colorcodes that are not used.
  * In method 'toString' a comment with a sysout is present, and maybe refactor the line into multiple lines for readability.

###### Parser.java  
  * Large amount of comments in method 'parse', showing all different values for the different splits. Also maybe clean up the way the Strings are split so that fewer lines are needed.
  * Comments at the bottom of the file that are no longer of use, since a description of the shape of the errors is given at the top of the corresponding classes.

## regextester
###### Regex
  Many comments containing thoughts at the end of lines.
###### RegexTester
  * Comments in the 'main' and 'readFileWhole' methods for testing the code.
  * Method commented out entirely at the bottom of the file.
