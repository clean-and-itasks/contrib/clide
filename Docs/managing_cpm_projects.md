# Functions of cpm and making .prj files for compiling projects

## Create Modules

- `cpm module {module name} create`
- this creates an empty icl and matching dcl file
- files are created in the current directory

## Create Projects

- in the folder where you wish to create the project type `cpm project {project name} create`
- the cpm will generate a .prj file, for this to work the current directory should include an .icl module with the same name as the project
- Alternatively cpm can create a new empty project: if no icl file exists an empty module with the same name as the project is created after confirmation in the command line

## Compile Projects

- open a console and navigate to the project folder
- type cpm project {project name} build
- this will generate an executable with the same name as the project if the project file exists and the project does not contain any errors

## Dealing with nested directories

- If you build a project that depends on a lot of modules in nested directories, then `cpm` automatically finds the required modules, and no special actions are needed.
- If you want to include a library that is *not* a subdirectory of your root project directory, then you need to add the path manually to the project. Let's say you have a library called 'testlib' a directory above your project root folder. Then in your project folder, you can manually add the path as such: `cpm project helloworld.prj path add ../testlib/` (Linux example).

## Adjusting heap and stack size

- The .prj file also contains metadata on the heap and stack size you want to use for your program. You can update your project file with this metadata as such: `cpm project helloworld.prj set -dynamics -h 2000M -s 20M`. Where 'h' refers to heap, and 's' to stack size.
- This update does not give any feedback when completed, but does work. This can be verified by visually inspected in the .prj file.

## Notes

- on windows cpm does not seem to accept paths as an argument when it expects a file, always navigate to the project folder and give the absolute path to the cpm
- RESOLVED: in the `cmd` you do not prepend .\ to indicate the current directory for some reason. If you leave it out, you can call a project file that resides in a different directory. E.g. when you are one directory above your project directory called "HelloWorld": `> "C:\Users\Edwin\Clean 3.0\cpm.exe" project HelloWorld\HelloWorld.prj build`. To be clear, you do *not* use `.\HelloWorld\HelloWorld.prj`.

