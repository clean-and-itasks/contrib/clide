# Internal guidelines for our Scrum team

This document contains notes and tips on the Scrum and Agile process, and the special functions of scrum master and product owner within the team.


# Checklist

Read the [the agile manifesto](http://agilemanifesto.org/), and read the [scrum guide](https://www.scrumguides.org/scrum guide). 

**Check**: do you know...

	* What a scrum master does
	* What a product owner does
	* what INVEST stands for
	* what NFRs are
	* the scrum pillars: Transparancy, Adaptation, Inspection
	* project tools, git and JIRA.
	* what a burndown chart is

# Scrum pillars

**Transparency:**

"Significant aspects of the process must be visible to those responsible for the outcome."

**Inspection:**

Scrum users regularly inspect scrum artefacts and progress towards a sprint goal to detect undesirable variance.

**Adaptation:**

If inspection shows it's necessary, adapt the process immediately (not the same as changing sprint goals).

Inspection and adaption occur in the four prescribed formal events of scrum:

- Sprint planning
- Daily Scrum
- Sprint review
- Print Retrospective

# Summary of Intro to scrum | by Steve Stedman https://www.youtube.com/watch?v=9TycLR0TqFAo

## Scrum vs waterfall

### Waterfall: 

- Plan
* Build
- Test
* Review
- Deploy

Problems with waterfall:

- Planning has to be done before project can start
- Planning is done without fully understanding the project
- Setback back up the waterfall require do-over, and takes up time unnecessarily.

### Scrum

Scrum is an implementation of *Agile*.

One cycle consists of Plan, Build, Test, Review.

For each cycle, a minimal amount of planning is done. The result is a "potentially shippable product" within a "incremental release" called a *sprint*.

Each sprint consists of 1 to 3 weeks, and sprints are repeated until the product is "feature complete".

#### Three key roles

1) Product owner

Product owner has the bright idea that turns into products. Responsible for defining the required features.

2) Scrum master

Servant leader to the team, protecting the team and progress, and leading meetings.

3) Team member

Can have various tasks.

#### Three artefacts

1) Product backlog
	- Prioritized list of features that could go in the product, known as "user stories".
	User stories are of the format: as a ... (e.g. user) I need ... (e.g. a feature) so that ... (a reason).
	This allows the product owner to estimate the right amount of detail for the size of the task.
2) Sprint backlog
	* Highest prioritized user stories. They are estimated for size (complexity, I guess is what he means), and are pushed onwards to the next sprint.
3) Burndown charts
	* Show the progress during the sprint on the tasks in the sprint backlog. Approaches 0 as the work is being completed.

#### Three ceremonies

1) Sprint planning
	* Product owner, scrummaster and team discuss user stories and estimate their sizes.
2) Daily scrum
	* What are you working on since last meeting, what is the progress, and what obstructions might occur (that you need help with).
3) Sprint review
	* At the end of the sprint, present product to product owner. Review process, and improvements needed to move forward.

### Scrum workflow

1) Product backlog built by product owner, prioritized items brought to team
2) Sprint planning: everyone discusses priority user stories and determine what can go in the next sprint
3) Result of sprint planning is sprint backlog with committed to user stories, which are now understood by everyone.
4) Sprint
5) During sprint daily sprint meetings
6) Product owner evaluates potentially shippable product, determines if it is shippable or needs more features.
7) Sprint review, show product to product owner
8) Retrospective review, team discusses how to improve their process.
9) Repeat workflow per sprint.

Uzility is a software tool for scrum.

# Tips and notes on being a scrum master

1) Be a servant-leader
2) Evaluate interactions with the scrum team and help everyone understand which interactions are useful
3) Help everyone change unproductive interactions
4) Help the team define when something is done
5) Know your team, their strengths and weaknesses, and dynamics
6) Keep everyone (team, product owner, stakeholders) up to date on the scrum process
7) Never commit the team to something without consulting them first
8) Stimulate an environment in which everyone sees each others as peers in the scrum process
9) Protect the team against overly demanding product owners or stakeholders: prevent cutting corners
10) But also protect the team against complacency: make sure everyone keeps improving
11) Praise and motivate where that is due
12) Encourage the team to take over your job: once everyone knows the Scrum process, your job becomes easier
13) Don't be afraid to let the team come up with their own solutions, stimulate independence
7) Review your own actions and ask if they are in line with the Agile manifesto

**Service to the product owner:**

- Ensure that goals, scope and product domain are understood by everyone
- Help find techniques for efficient backlog management
- Stimulate the formulation of clear and concise product backlog items by the team
- Ensure the product owner has everything he needs to prioritize backlog items to maximize value
- Facilitate scrum events when needed

**Service to the Dev team:**

- Lead and coach the team in its Scrum adoption
- With a focus on *empirical* product development: dealing with the day to day reality of it
- Cause chances that increase productive in the Dev team


# Tips and notes on product ownership

1) Know how to measure business value
	* E.g. in terms of KPIs, or Key Differentiators
2) Be an expert on your product: know your customers and competitors and know what your value proposition is
3) Focus on 'what' and 'why' more than on the 'how' (technical difficulties and design are the focus of the team)
3) You are part of the team and equal to everyone
4) Be approachable and always know what everyone is up to
5) Don't do "Big Bang delivery", but apply the Minimum Viable Product (MVP) strategy: done is better than perfect.
6) Learn techniques on how to prioritize backlogs, such as MoSCoW, Kano Analysis, or The 100-Dollar Test.
	* Prepare a product roadmap for the big picture (but be aware that the backlog is a living artefact)
	* Visualize the backlog, sprint progress and dependencies between user stories
7) Learn to say NO: don't let backlogs get too big
8) Something you say no to today, might be valuable in a month, but don't maintain a separate backlog for these ideas
9) Consider letting the team help in managing the backlog with smaller tasks: describing user stories, acceptance criteria, etc. so you can focus on the long term goals, stakeholders, and business value.
10) Always be aware of what is on the backlog (be careful with letting everyone add stuff to the backlog: this reduces transparency)
11) Order the backlog continuously
12) The backlog does not have to be complete from the start: the whole project does not have to be clear, just keep focussed on the product instead
6) Keep stakeholders informed and receive feedback from them
	* But you as product owner are responsible for making final decisions on how to increase the value of the product
14) Maximizing value is not the same as increasing the number of features
7) Be an "intrapreneur"
8) If necessary, do not be afraid to remove features that will never or rarely be used. It is probably an inefficient allocation of resources.
9) You cannot prepare for every scenario, focus instead on mastering how to maximize value on the go, in changing circumstances
10) The only boss is the customer, think wisely on how to spend his/her money.


# Summary of Agile Product Ownership | Henrik Kniberg 2012

A product owner has a vision for a product, what problem it's going to solve and for who

Stakeholder: users or people that will be effected in some way by the product

User stories: the product owner together with stakeholders are able to sketch up user stories.

A dev team: develops user stories, with a particular capacity (e.g. per week). amount of "story points".

Because the release cycle is pretty intense, the dev team invests heavily in *automated testing*. unit tests for the code, but also every user story has at least one test on the feature level.

It is important to understand the capacity level of the team (e.g. how many user stories can be implemented per week?).
For each user story that is delivered, the stakeholders likely come up with new user stories. 
A selection has to be made to match the capacity to prevent overflow of the dev team, causing inefficient multitasking and demotivating.

So the team indicates their unit stories capacity (wip limit) and the product owner selects the most important user stories. 
Whenever a user story is finished, the dev team accepts a new story, but never beyond their capacity.

A side effect of this approach where a lot of user stories come in, but only a few can be processed at a time, is that there will be a queue of user stories. 
This is called the **product backlog**. 
This queue needs to be managed by the product owner, who needs to prioritize and say **no** to a lot of user stories.
The most important function of the product owner is in a sense to say no within this workflow.

The process of prioritization occurs in communication with the product owner, stakeholders and dev team. 
This type of meeting is called "backlog grooming" and consist of making estimations, splitting stories, describing acceptance tests.

There is no correlation between story size and story value. In order words, it makes sense to prioritize higher value stories when they are of similar size, taking into account the law of diminishing returns. 
Size and value is a guessing game, done in dialogue. For user story *value*, the product owner is mostly in dialogue with the stakeholders. For story *size*, the dev team can help making a realistic estimate.

Initially, the estimations for size and value will initially be off, but due to the short feedback cycle you can quickly make better estimates in the agile system.

Also, prioritize very clear bits of stories that can be quickly implemented, and delay vaguer stories to a later phase where more about the project is known. 

## Trade-offs

- business risk: do we build the right thing?
- social risk: can these people build it?
- tech risk: does it scale?
- cost+schedule

There is a trade-off between risks/uncertainty and knowledge. The more risk, the more knowledge acquisition is required. In the phase of knowledge acquisition, the customer value stays relatively low. However, this phase is necessary to gain a linear increase in customer value later. At the other end of the curve, there is the phase where all core features are already built and the curve flattens out. In this phase, the tail can be trimmed:

> value = customer value + knowledge value

A second trade-off is between:

- build the right thing (focus of product owner)
- build the thing right (focus of dev team)
- build it fast (focus of scrum master/agile coach)

It is hard but crucial to find the right balance. In agile, different parties negotiate with each other and represent these aspects.

A third trade-off:

New product development vs old product improvement.

A product is never really finished. Maintenance of older products has to be integrated and balanced into the product backlog.

## Expectation management

The product owner has to do expectation management to balance out the expectations from all different stakeholders. Formulating realistic expectations can be aided by making a story burnup chart with "story points", both with a optimistic and pessimistic trend. This can be used to manage expectations given on the experience and velocity so far. Note that the burnup chart is about output, not per se outcome. 

The difference between the pessimistic and optimistic trend show the uncertainty projected into the future, based on the variability of the trend so far.

The stakeholders can ask:

- at what time is this feature done? (fixed feature axis question)
- how much will be done by christmas? (fixed time axis question)
- is this feature done by christmas? (fixed feature and time question)

When the answer to the last question is: "no" we cannot do that feature at that time, always make sure to offer a realistic expectation in return. 

It is usually better to reduce scope than to increase time, i.e. "at that time i can deliver this instead" vs "i can only deliver that later".

N.B. projection into the future only works if the dev team maintains a sustainable pace. in turn, to achieve this, the product owner should never promote taking shortcuts to satisfy stakeholders quickly. 

In larger teams you would have more product owners, who also need to communicate with each other to synchronize the processes in case there are dependencies between teams and product aspects. this potentially requires a "chief product owner".

# List of sources (that are not mentioned yet):

- https://medium.com/@lukkrz/top-13-tips-to-become-a-great-product-owner-bb366c678a84
- http://blog.crisp.se/2016/01/25/henrikkniberg/making-sense-of-mvp
- https://www.scrum.org/resources/blog/10-tips-product-owners-product-backlog-management
- https://www.mountaingoatsoftware.com/blog/ten-sentences-with-all-the-scrum-master-advice-youll-ever-need


