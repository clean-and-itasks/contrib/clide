# User manual: How to set up CLIDE
Welcome to the user manual of how to set up CLIDE. We are very pleased that you decided to use CLIDE as a platform for the programming language Clean. 
CLIDE is an Eclipse plug-in that is made by students at Radboud University. 

## Installing CLIDE
You can find us at the Eclipse Marketplace.

## Installing Clean
You can find Clean at https://clean.cs.ru.nl/Clean where there should be a big download button. 
You can download Clean in any folder you want on your device.

## Linking CLIDE and Clean
When you open CLIDE, the first thing you should do is make sure that CLIDE knows where to find Clean.
This can be done by *CLIDE Settings* in the menu bar on top. From there, click *Set CPM* and find the file `cpm.exe` where you downloaded Clean.
Now you are all ready to start programming!

## The first time
Now, first times can be exciting but we made it pretty easy for you. 
Write your Clean file however you want it. Are you done?
Then there are two options to compile and run your file:
1. Right click your file in the Package explorer, click *Run as* and then click *Clean launcher*.
2. Make sure that the correct file is selected in the Package explorer and click the green Run button in the bar below the menu bar.

## More advanced run options
You can find more advanced run options in the *Run configurations*. This can be found by clicking *Run as* and then click *Run Configurations...*.