# Summary of product owner meeting with Franc Grootjen | 2018-10-15

## Motivation for the project

The CLIDE project concerns the development of an IDE for the functional programming language CLEAN, taught at Radboud University Nijmegen. 
A main motivation for this project is that currently some aspects of the functional programming course are somewhat questionable. 
The main annoyance is that the existing CleanIDE is only available on the Windows operating system, which causes issue because a significant portion of students interested in programming run Linux and MacOS. These students are either forced to do their course work on university computers only, or have to take a more complex route: 1) use a text editor of choice (no IDE), 2) use the clm.exe executable for compiling Clean files (.icl, .dcl, .project), 3) then sequentially edit the Clean script, save it, compile it, read potential error messages, and repeat. 

Both options offer clear limitations. A clear limitation of the latter option is that a lot of initiative and knowledge is required from the students to make things work. And even then, editing and the compile/run/debug procedure are completely separate from each other. 

This situation has been unchanged for several years. On top of that, documentation of Clean is rarely updated (the last version is from 2011). The same seems to hold for Clean itself. In other words, little initiative is expected from the side of the developers of Clean to solve these problems. Therefore Franc took initiative himself and proposed the development of a Clean IDE, with the proposed name CLIDE, as a student project.

From this I distill two main requirements of such an IDE:

1) It must be cross-platform, i.e. at least run on Windows, Linux, MacOS.
2) It must integrate editing and compiling and running a Clean file (and if possible: debugging)

## Preferences

The first requirement mentioned above (cross-platform) constraints the use of programming languages: Whatever option we choose, it must be portable.

I asked for Franc's ideas for the project. 
The original project description suggested two rough options: develop the IDE from scratch, or develop it as a plugin/ extension to an existing IDE, in particular Eclipse. 
For both possibilities Franc pushed Java forward as the preferred language. We know that Java is a favorite language of Franc, but throughout our conversation two advantages of Java became apparent:

1) Java's JVM is portable
2) Java is a "proper" programming language for a project like this

I expressed the latter point quite vaguely for brevity, but let me clarify: Franc wanted to ensure our project will become a real programming project, and not "hacking" a text editor. Java is also a language we are all familiar with. On top of that, Java is also a good all-round language in which we are guaranteed to be able to do what is required.
These considerations are thus quite pragmatic.

In our conversation it quickly became apparent that Franc's main focus and expectations all revolved around building a plugin for Eclipse, rather than building from scratch. This is because:

1) Eclipse is a plugin ecosystem that should facilitate all aspects of language support we need. There is a community in which people certainly have done similar projects for other languages.
2) Eclipse is widely used and freely available, and a solid choice for use in the functional programming course.
3) Eclipse is written in Java, thus benefiting from Francs preference for Java.
4) Writing the IDE as a plugin for Eclipse will increase maintainability of the IDE. Because Eclipse facilitates writing of plugins naturally, we can expect that the plugin we write will also work in future version of Eclipse.

Compared to other IDEs that we considered (i.e. Netbeans), I think point 1 and 4 in particular are quite decisive. There is however a clear concern that Franc shared, and that also came up in Johannes' research into Eclipse: precisely because Eclipse is quite a big ecosystem, we can expect a significantly steep initial learning curve. Franc suggested to take at least a week to do research into building a plugin for Eclipse.

N.B. Franc himself has not build a plugin for Eclipse. But considering that Eclipse is a plugin ecosystem, there should be plenty of support and guides online. Our research will show if this assumption is true. 

With Nick's concern in the back of my head that we should choose the project such that Peter Achten will also be interested in it, I explicitly asked if people from computer science would use Eclipse. Franc responded affirmatively, and indicated that he received interest out of that direction.

I also brought up other options we explored. IDE-wise, Eclipse really is the obvious choice. Netbeans falls short when it comes to plugin and language support. I made a case for heavily extendable text editors and mentioned Atom and Visual Code. Franc still clearly preferred a "real" IDE, but was open to the idea, in the sense that it could be a very good backup option if we come to the (unlikely) conclusion that Eclipse really will not work out.

Other than that, Franc clearly expressed that he would rather have a minimal but done product, than a non-finished product. I think this is an additional and strong argument why the plugin approach is preferred over the "start from scratch" approach. We thus ultimately reached a "hierarchy of preference":

1) The most preferred option is writing a plugin for Eclipse
2) But Franc is open to making a plugin for a extendable text editor, such as atom, if the first option falls through.
3) Building from scratch is least preferred, as it has the lowest probability of producing a finished product

## Requirements

Throughout our talk, we discussed some requirements for the product. I will simply list them here, without prioritizing them yet.

Minimum requirements:

- Different types of windows, such as project and editor view
- Build/run (debugging wasn't mentioned and is probably too ambitious)
- Loading, editing, saving Clean files

Other/optional:

- Underline errors
- Make cursor jump to errors
- (Requires parsing of Clean error messages. Franc even suggested that if they are too unreadable, we could edit the source code of Clean, but this seems a sub-optimal solution. Dan had a better suggestion for this issue.)
- Syntax highlighting*

\* Franc said that syntax highlighting has a very low priority for him, and that it takes a lot of time. 
I mentioned that we actually found plugins already that dealt with syntax highlighting (using regex, if I recalled correctly). Before I could suggest it myself, he suggested reusing those.

One suggestion Frank made for early sprints, is to focus on getting the IDE to work for one filetype (Reminder, we have .icl, .dcl, and .project files).

## Other

We discussed practical issues concerning scrum, in particular how to log time for the special functions of scrum master and product owner by proxy, and how to balance that function with writing code. In short: those functions are really dedicated, and in principle it is not required to write code at all (in practice, me and Dan do want to write code, and are unlikely to spend 100 hours on our dedicated function alone).

## Recommendation

- Start a short research sprint on how to write a plugin for Eclipse
