# Useful Links

This document is meant to provide a quick overview of useful links that were found and can be used for reference / fixing bugs. 

## Bug Fixes

Missing Source Files in Eclipse - only generic extensions in PDE
https://stackoverflow.com/questions/5575932/eclipse-plug-in-editor-extension-tab-show-generic-in-new-submenu

Some common errors + basic tutorial but with more explanation than the cheat sheet in Eclipse
http://www.vogella.com/tutorials/EclipsePlugin/article.html#adding-eclipse-4-x-parts-to-eclipse-3-x-applications-via-the-code-org-eclipse-ui-views-code-extension-point

## Useful Links for Reference
