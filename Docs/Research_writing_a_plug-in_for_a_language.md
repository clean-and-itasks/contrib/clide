### Research Writing a Plug-in for a Language
 
## IBM Paper

The "Create a commercial-quality Eclipse IDE" by IBM offers a good insight into the mechanics behind building an IDE plug-in for Clean. Therefore we can definitely use it if we are unsure about how different parts of an IDE come into play and what processes we need running on what threads.

However it is not entirely up to date as the tutorial is dated 12 years ago.

Therefore I went on looking for a tool that would allow us to specify the Clean language without having to create everything from scratch.


## XText

Since then an open-source software framework called Xtext was created of which stable release happened on February 1st of last year, therefore much more up to date.

Checking the functionality it offers a lot more than we are going to need:

* Syntax coloring
* Code completion
* Static analysis
* Outline view
* Source-code navigation
* Indexing
* Compare view
* Hovers
* Code folding
* Rename refactoring

It also has a lot of basic tutorials, a forum, and documentation on their website, for if we get stuck, and it provides a lot of automatic services. We just have to provide the grammar and other Clean-specifics, of which I am not really sure how to do that yet.

There is a video in which an introduction and overview is given on XText, in combination with a short tutorial of a basic domain-specific language. 

Although it seems that the focus lies on DSLs, this piece can be found on their website:
"Xtext can build full-featured text editors for both general-purpose and domain-specific languages. In the background it uses the LL(*) parser generator of ANTLR, allowing to cover a wide range of syntaxes. Xtext editors have already been implemented for JavaScript, VHDL, Xtend, and many other languages." 

So in short, there is plenty of functionality that XText offers, but it might be a bit too much to learn for us as we want to just have basic functionality.

## IMP

I also came across Eclipse's IMP (IDE Meta-tooling Platform) on stackoverflow:

"It is unfortunate that IMP indeed seems abandoned, as it covers exactly your scenario: your language, compiler, etc. is working already and now you need "just" the IDE. Nevertheless, IMP is still working, and recently, it has been used to implement the IDE for the Frege programming language." https://github.com/Frege (13-10-2012) 

From the IMP github(https://github.com/impulse-org):

"IMP radically simplifies and speeds up the IDE development process in Eclipse, for both language with existing front-ends as well as languages generated using compiler and interpreter generation frameworks"

There is a lot to find on the GitHub, and in combination with the implementation and documentation of Frege which is also a functional programming language, we might have a good example to work off of.

## Conclusion

In conclusion: Although I still have to spend more time reading in on IMP, it seems as a more basic framework than XText, which probably suits our goal more. However depending on how easy it is to implement basic functionality on XText it might be a better option if we feel that we want more complex functionality.

https://www.eclipse.org/Xtext/
https://wiki.eclipse.org/IMP


