> Anouk Prins

# Research on a language plug-in in Eclipse
For most of the information here I used [this paper](https://www.ibm.com/developerworks/opensource/tutorials/os-ecl-commplgin1/os-ecl-commplgin1-pdf.pdf).
## Different components of an IDE 
We will need a **Core**, which will act like the base layer of the IDE. It needs to have some sort of parser and lexer that can understand the language and a symbol table. On top of this, an **UI** is built. This is where syntax highlighting, etc. is happening. Other components that are mentioned are the **Debugger**, **Documentation** and the **Build**.  [This article](https://www.ibm.com/developerworks/opensource/tutorials/os-ecl-commplgin1/os-ecl-commplgin1-pdf.pdf) recommends to have the Core and the UI seperate if you have a large and complex IDE, but that you can combine them into one Java Archive (JAR) if you have a smaller language. JAR is a package file format used to aggregate many Java class files (plus metadata) into one file for distribution. It is important that we make a decision on this, since we can probably only start working on the UI after the Core is finished if we go for the first option. This is because when bugs appear in the Core, it is almost impossible to work around that in the UI.

## Creating a Core
As mentioned above, we will probably need a **Parser**, **Lexer** and a **Symbol table**. 
### Symbol table
This is a table stores information about the language. Parts of the IDE use the Symbol table to find out about the code and perform their actions. There are different implementation possibilities of the Symbol table like: [Source](https://www.geeksforgeeks.org/symbol-table-compiler/)
* List: Insertions is fast, but lookup is slow for large tables (which I expect we will have)
* Linked List: A pointer is maintained to point to the first record of the table. Again, Insertion is fast but lookup is slow for large tables.
* Hash Table: Most commonly used for Symbol tables. Insertion and lookup can be made very fast. However, hashing is complicated to implement.
* Binary Search Tree: Running time depends on how you build the tree but are O(logn) on average.

[More info on Linked Lists, Hash Tables and BSTs used as Symbol Tables.](https://introcs.cs.princeton.edu/java/44st/)
### Parser and Lexer
Many are readily available: 
* Flex (Fast Lexical analyzer generator): It generates a lexer but it is only available for C and C++.
* Yacc (Yet Another Compiler-Compiler): outputs in C and is made for the Unix operating system. Together with Flex it is available in Linux distributions as Bison.
* JavaCC (Java Compiler-Compiler): According to their own website, the most popular parser generator for use with Java Applications. It also provides tree building, actions, debugging, etc. It is similar to Yacc in that it generates a parser from a formal grammar written in EBNF notation. 
* Anagram: Generates platform independent C or C++ code. Is not for free and website is ancient. 
* ANTLR (ANother Tool for Language Recognition): Takes as input a grammar and generates a source code as output. It supports Java and the grammar needs to be context-free which is expressed using Extended Backus-Naur Form (EBNF) (More info needed). It can generate lexers, parsers, tree parsers (does this mean no Symbol table needed?). Apparently there are plug-ins for syntax highlighting, syntax error checking and code completion on the website of ANTLR. Also: ANTLR Studio is an IDE ANTLR which plugs into Eclipse.

### Incremental or nonincremental parser and lexer
Also, we need to decide whether we want an incremental or nonincremental parser. Incremental means that after every keystroke the parser and lexer are invoked, scanning the entire document while forming a tree of all the code in the editor. This is nicer for the user but for us, it will be incredibly though and complex. It will add huge amounts of complexity to our code.

It is also possible to have a partially incremental IDE. This means that for example only the lexer is incremental. In this way, the IDE is still fast but not as complex. NetBeans takes this approach for example.

### Other important notes on the Core
The core needs to be fast and according to the IBM paper it will be the biggest challenge while designing the IDE. The art of IDE design involves keeping your data structures up to date while maintaining the IDE's responsiveness. A possible approach:
1. Run the parser/lexer in a background thread
2. The thread runs after a fixed, short time interval and runs the parser/lexer to update the smbol table
3. The other views run in their own thread and update themselves by looking at the symbol table bu their interval time is longer than the background thread.

This way you're *mostly* up to date and the IDE is fast enought that the user won't notice. However, ANTLR Studio does this for you and is *always* up to date.

You must make sure that the parser can recover from errors that the user typed. ANTLR has basic error handling but you must manually put checks in the parser to make it recover from the errors. This goes for all IDEs. 
## My thoughts
I think it makes sense to divide the Core and the UI. I would guess that CLIDE will be more clear to other people who might be working on it later and would want to add features to CLIDE. On the matter of the Symbol Table, I would go for either a hash table or a BST. Both ANTLR and JavaCC seem good options. I think it is also useful to look into ANTLR Studio. I am not sure on whether we should have an incremental parser and lexer or not. A partially incremental approach seems like a nce middle road.