# Summary Review meeting 2018-11-15 with Franc Grootjen present

Franc Grootjen attended the Review meeting at the end of CLIDE Sprint 3.

We showcased the result of Sprint 3: A Eclipse instance running a basic plugin, that is able to load, save and edit .icl and .dcl files.

That means we accomplished our Sprint goal, which we defined in Jira as "Having a basic plugin able to load, edit, save .icl and .dcl files." Franc agreed, and was enthusiastic about the progress.

We inspected the burndown chart of Sprint 3 together with Franc, with which he was also satisfied. It shows a approximately linear increase in hours logged.

We asked Franc for his wishes for Sprint 4. In particular, we emphasized that we had trouble parallelizing work in the last sprint.

Franc suggested that next to a high priority goal, we could already start looking into smaller features that are (relatively) independent.
He mentioned that it would be handy to already inspect the output of the Clean compiler so that we can later start implementing features related to for example jumping to errors, or providing clear error messages.
Another option that was discussed, with very low priority, is to make existing CLIDE aspects prettier, e.g. applying styling to different files, or custom buttons.

When it comes to clear error messages, Franc brought up again that we could in the worst case edit Clean source code if we need different error formats. We instead suggested that for such functionality we prefer to explore the option to integrate CLIDE with existing resources that contain information about Clean errors, such as Cloogle. Franc was open to that idea. 

Also he suggested dividing a big goal, such as being able to compile a Clean file from within CLIDE, into smaller sub goals that are a bit more independent. 

We reached the following wishes / requirements for the coming sprint:

**High priority:**

- Being able to run external commands from within CLIDE, so that we can call the compiler.

**Side task:**

- Inspecting Clean output as a preparation 

**Wishes for the future**:

- Clear error messaging
- Jumping to errors
- "Prettify" CLIDE
