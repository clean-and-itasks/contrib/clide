# Git-workflow Research
> Johannes-Lucas Loewe

## Introduction
Git-workflows are about the way Git is integrated into project work. A such every team and project needs a different Flow i.e. there is no one-fits-all solution.
Therefore obviously one can also not say which is the right one for us, and this decision needs to come from within the team. It is possible though to state the factors having the biggest influence on this, in usual cases. So ask yourself these questions before reading on:

- who do we trust?
- where lies the responsibility? i.e. who has to account for faults?
- do you want power and responsibility for yourself as a normal developer?
- are we working ion features together or alone?
- so we have a main-framework that needs to stay up to date, or is the project going to be highly modular?

keep these in mind when thinking about the Pros and Cons of the workflows.

## Merge Requests

Merge requests are basically gits way of putting a hierarchy on the Teams permission. They are created to ask other persons to verify the changes you made, and always occur when 2 branches are being merged, hence the name. So e.g. junior-developer Johannes can ask senior and experienced developer Nick to verify the changed he made before allowing them to be applied to the project, e.g. on master. In that case the branch 'johannes' would have been merged into 'master' by Nick, through a mergerequest Johannes created. 

## The Flows
In the following I will describe what i deem necessary to know about the most common flows, if something is unclear feel free to ask me or search in my references.

### Master Only
The most simple of worklfows is the master-only. In that case everyone does always push to master[branch]. It makes developing easy but is obviously dangerous and does not allow for added-control. 

> I think it is **unfeasible** for our situation, since it basically is a all work on it with rollback possibility project setup. 

### Feature Branch
Feature-branch basically means that for every feature that is created, a new branch of the main project is made. It has the advantage that every feature is basically treated modular, meaning it can be tested and merged separately. But it can also lead to deadlocks or cumbersome code, if the features are small.

#### Feature Branch with Merge requests
Merging back into the main project [e.g. branch master] can be done by merge-requests, and therefore usually need to be verified - if wanted - which makes sure that there is no random mess up [most of the time].
If master is protected it means that only experienced people can merge. Be aware though that this also usually leads to longer development to 'shipment' times, since if the people with rights are few it will inevitably be a bottleneck.
### Developer Branch
The idea is the same as with feature, but instead each developer gets its dedicated branch to develop a feature on. This is basically the ultimate control version, and makes a lot of sense if people work separately on medium-sized features and merging into the main-project is done less often.
In this case also each branch would be protected so only the developer and admins can push to it, making it save to use also with huge teams. 

### Gitflow

### Gitflow ala webC
Gitflow usually adapts the feature-branch approach [but may as well be used with the developer branch]
The main thing it is adds a layer of security and control by adding a branch usually called develop. This branch contains always the latest bunch of features is used to gather them and test them. If it is ready it is the only branch that is ever merged into master, which would basically be a release.
Also different right between merging master and develop can be distributed making it very adaptable. 

This is used by the webC.

### Git-flow in real
The 'Real' gitflow also has an Release branch, so master is the current project state and Release it what is delivered. 
It is used so that hotfixes are made in master, and then can be merged into release quickly. After each hotfix Release therefore needs to also be merged in Develop.
Develop is merged into the Release branch [when released ;)]
Afterwards Release is merged into master.
This is really adaptable and basically makes everything really well controllable and still flexible. But it requires expertise to use and can blow up the whole project if used wrongly. 

![GitFlow](./images/GitFlow.jpg)

### Skullcandy’s workflow

Basically the webC flow, but everything is merged into develop and master. This adds control as to what need to be where and makes hotfixes a lot easier, but if you screw up once you can get out of sync and then stuff happens thats not supposed to.

## What it means for us
First: we have a lot of choice, and we are not limited to the ones mentioned, we can make up our own as we go. 
\
Secondly: we will need to decide on roles and the trust we put into them. 

Thirdly: we will need to make trade-offs between verification and speed of the project. It is important we decide on our priorities before we start using one or the other.

## My Advice

In my opinion the most important thing is that we get started on a structure how we want to develop. As soon as we know what our priorities are we can decide if are going to work as separate entities on big or small features, and how much control we need. generally i would think the webC is as complicated as we need, i would even say it is too much. It might rather be worth making a branch for each main feature and then sub-branching of these for subfeatures.

But in the end we need to decide that together and unfortunately only time will tell if we chose the right one. 
The thing that is the most important is that once we chose one, we stick to it as good as possible.

## References

Feel free to read in these for more detail on the workflows (and nice pictures, they really help).


[Wokflows 1](https://www.endpoint.com/blog/2014/05/02/git-workflows-that-work)

[Wokflows 2](https://buddy.works/blog/5-types-of-git-workflows)

[Gneral Git Practices and Flows](https://documentup.com/skwp/git-workflows-book)