# Instructions for using cpm on Linux

---
Info: assumes that you already have a project file
Result: a binary that runs your project
---

1) Make sure cpm is in your path variably so you can call the command from command line in whatever directory your project is in. (I suggest that for integrating cpm in the IDE we have to tell Eclipse manually where cpm is located on your system)

Example: `export PATH=\$PATH:/home/edwin/Documents/AI/clean3/bin`

2) Navigate to the folder where your .prj file resides. 
3) To build the project: `cpm project helloworld.prj build` or shorter: `cpm helloworld.prj`. This links together all .icl and .dcl files specified in the project file, and creates an binary executable `helloworld`.
4) You can now run the executable by typing in `./helloworld`
