# Getting started

We have .project and .classpath files included now, so you can import the project into Eclipse. 

File --> Import --> Existing Projects into Workspace  

or:

File -> Open Projects from File System ...

The develop branch does not have a src folder yet.
After importing the project:

1) Right click on the project (Clide)
2) Build path
3) New source folder
4) Call the source folder src
