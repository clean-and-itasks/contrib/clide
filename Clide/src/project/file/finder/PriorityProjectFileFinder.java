package project.file.finder;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;

public class PriorityProjectFileFinder {
	/**
	 * This class finds files in the projects of the workspace, 
	 * Prioritising open projects, in order to find the best match.
	 * 
	 * This is not static since open projects change over time and therefore not much work could be saved
	 * 
	 * TODO: we may want to implement that the project with the most recent opened file is used,
	 * or similar
	 */
	private LinkedBlockingQueue<IProject> projects = new LinkedBlockingQueue<IProject>(); //stores all projects
	private String file = null; //stores target file

	/**
	 * Create a FileFinder for projects, can be used to find resources with uncertain origin
	 * @param toFind name if the file to be found
	 */
	public PriorityProjectFileFinder(String toFind) {
		//set file this finder is created for
		this.file = toFind;
		//Add the projects, first all open ones, then the rest.
		ArrayList<IProject> allIProjects = new ArrayList<IProject>();
		for (IProject p : ResourcesPlugin.getWorkspace().getRoot().getProjects()) {
			allIProjects.add(p);
		}
		for (int i = 0; i < allIProjects.size(); i++) {
			IProject p = allIProjects.get(i);
			//add open projects, remove them from the project list
			if (p.isOpen()) {
				projects.add(p);
				allIProjects.remove(p);
			}
		}
		//add the last projects
		for (IProject p : allIProjects) {
			projects.add(p);
		}
	}

	/**
	 * Get the file from the FileFinder based on values it was initialised with
	 * @return the file as a Java IO file, null if not found
	 */
	public File getFile() {
		File found = null;//return file
		//as long as there are projects to be searched
		while (!projects.isEmpty()) {
			//get first Project
			IProject currentProject = projects.poll();
			//get the location
			URI loc = currentProject.getLocationURI();
			
			//use new java file functionality to collect all files
			//i.e. For all folders in the project, filter only real files, collect them as a list
			try {
				java.util.List<Path> resourceResult = Files.walk(Paths.get(loc)).filter(Files::isRegularFile).filter((f)->{
					String fn = f.toString();
					if (fn.endsWith(file)) {
						return true;
					}
					return false;
				}).collect(Collectors.toList());
				//if something was found, assume the first file to be the correct one
				if(resourceResult.size() >0) {
					found = resourceResult.get(0).toFile();
				}
			} catch (IOException e) {
				System.err.println("Problem searching for file to be linked.");
				e.printStackTrace();
			}
			//if something was found, we are done
			if (found != null) {
				break;
			}
		}
		return found;
	}
}
