package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;

import org.eclipse.core.internal.runtime.Activator;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * 
 * @author Clide 2018-2019
 * 
 * First Page used by the "cleanProjectExistingFilesWizard". Defines the
 * layout of the first wizard page in which the user enters mandatory information that
 * is needed for project creation.
 */
public class CleanProjectWizardExistingPageOne extends WizardPage {

	Text mainIclLocation;
	Text projectName;
	
	ModifyListener modifyListener = new ModifyListener() {			
		@Override
		public void modifyText(ModifyEvent e) {
			setPageComplete(isPageComplete());
		}
	};
	private CPMSelector cpmSelector;
	private ProjectNameSelector projectNameSelector;

	
	protected CleanProjectWizardExistingPageOne(String pageName) {
		super(pageName);
		setTitle("Clean Project Wizard");
	}

	/**
	 * Defines the layout of the wizard page. The wizard page includes
	 * - a row to choose the main ICL file location (with browse button)
	 * - a row to pick a project name
	 * - a row to indicate the row of the CPM to be used for project creation (with browse button)
	 * Changes to any of the text fields leads to the pageComplete status being updated
	 */
	@Override
	public void createControl(Composite parent) {
		//Main Composite, content ordered in a grid
		Composite composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout(3, false);
        // used to set size of text fields
        GridData textLayout = new GridData(WizardConstants.TEXTFIELDWIDTH, WizardConstants.TEXTFIELDHEIGHT);        
        composite.setLayout(layout);
        setControl(composite);
		
        // Main ICL Location Row
        new Label(composite, SWT.NONE).setText("Main .icl location");
        mainIclLocation = new Text(composite, SWT.NONE);
        mainIclLocation.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				setPageComplete(isPageComplete());
				if(projectName != null && projectName.getText().equals("")) {
					File iclFile = new File(mainIclLocation.getText());
					if(iclFile.exists()) {
						File parent = new File(iclFile.getParent());
						projectName.setText(parent.getName());
					}
				}
			}
		});
        
        mainIclLocation.setLayoutData(textLayout);
        mainIclLocation.setText(ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString());
        mainIclLocation.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				setPageComplete(isPageComplete());
			}
		});
        Button browseIcl = new Button(composite, SWT.PUSH);
        browseIcl.setText("Browse");
        browseIcl.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(getShell());				
				//file dialog defaults to workspace location
				fd.setFilterPath(mainIclLocation.getText());
				String[] allowedExtensions = {"*.icl"};
				fd.setFilterExtensions(allowedExtensions);
				mainIclLocation.setText(fd.open());
				setPageComplete(isPageComplete());
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				return;
			}
		});
        
        // Project Name Row
        projectNameSelector = new ProjectNameSelector(composite, textLayout, this);
        
        
        // CPM Location Row
        cpmSelector = new CPMSelector(composite, textLayout, this);
        
        composite.getShell().pack();
	}
	
	/**
	 * checks whether all necessary information for project creation is present and valid
	 * return true if:
	 * - icl file exists
	 * - icl file is valid (has .icl ending)
	 * - cpm is valid
	 */
	@Override
	public boolean isPageComplete() {
		/*
		
		 */
		if(mainIclLocation == null)
			return false;
		else {
			File mainIclLoc = new File(mainIclLocation.getText());
			if(mainIclLoc.exists()) {
				if(isICLFile(mainIclLoc))
					return true;
			}
			return false;
		}
	}


	/**
	 * checks whether a file given as parameter is an icl file.
	 * 
	 * @param mainIclLoc the File to be checked
	 * @return true if the file exists and has .icl ending
	 */
	private boolean isICLFile(File mainIclLoc) {
		if(mainIclLoc.isFile()) {
			String name = mainIclLoc.getName();
 			return name.substring(name.length()-4, name.length()).equals(".icl");
		}
		return false;
	}
	
	public String getCpmLocation() {
		return cpmSelector.getLocation();
	}


	public String getMainLocation() {
		return this.mainIclLocation.getText();
	}
	
	public String getProjectName() {
		return projectNameSelector.getProjectName();
	}
}
