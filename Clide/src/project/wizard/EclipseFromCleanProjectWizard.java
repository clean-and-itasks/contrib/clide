package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

/**
 * 
 * @author Clide 2018-2019
 * This class implements a wizard that allows the user to create a new 
 * Eclipse project that is opened in the current Eclipse Workspace.
 * This wizard assumes existence of a Clean Project that should be 
 * added to the Eclipse project
 */
public class EclipseFromCleanProjectWizard extends Wizard implements INewWizard {

	EclipseProjectWizardPageOne pageone = new EclipseProjectWizardPageOne("Page One");

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		return;
	}

	/**
	 * called when the user clicks "Finish". Collects relevant information entered in the wizard
	 * and calls ProjectGenerator to create an Eclipse Project and open it.
	 */
	@Override
	public boolean performFinish() {
		File prjPath = pageone.getPrjFile();
		File prjFolder = prjPath.getParentFile();
		String projectName = pageone.getProjectName();
		if(projectName.equals("")) {
			projectName = prjPath.getName();
			projectName = projectName.substring(0, projectName.length()-4);
		}
		System.out.println("Creating Project: \nProject Name: " + projectName + "\nProject Location: " + prjPath );
		ProjectGenerator.createEclipseProject(projectName, prjFolder);
		return true;
	}
	
	public void addPages() {
		addPage(pageone);
	}
}
