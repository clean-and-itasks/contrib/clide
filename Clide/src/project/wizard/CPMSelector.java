package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;

import org.eclipse.core.internal.runtime.Activator;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Text;

import preference.ClidePreferenceConstants;


/**
 * 
 * @author Clide 2018-2019
 * 
 * Defines a user interface to be used in the project wizards. Instantiating this class creates a row
 * in an SWT user interface with a label, text field and browse button that lets the user select the location
 * of the CPM used for clean project creation.
 *
 */
public class CPMSelector {
	
	private Text cpmLocation;
	private final WizardPage wizard;
	
	private ModifyListener modifyListener = new ModifyListener(){
		@Override
		public void modifyText(ModifyEvent e) {
			wizard.setPageComplete(wizard.isPageComplete());
		}
	};
	
	/**
	 * Assess whether the text field contains the path of a valid CPM file.
	 * 
	 * @return true if the specified location does exist and the file name is either cpm.exe or cpm
	 */
	public boolean checkCPM() {
		if(cpmLocation != null) {
			File cpmLoc = new File(cpmLocation.getText());
			if(cpmLoc.exists())
				return cpmLoc.getName().equals("cpm") || cpmLoc.getName().equals("cpm.exe");
		}
		return false;
	}
	
	/**
	 * @param parent 		the composite that the row should be added to
	 * @param textLayout 	layout for the text field
	 * @param wizard		the wizard page that the row is to be added to
	 */
	public CPMSelector(Composite parent, GridData textLayout, WizardPage wizard) {
		this.wizard = wizard;
		
		 // CPM Location Row
	    new Label(parent, SWT.NONE).setText("CPM Location");
	    cpmLocation = new Text(parent, SWT.NONE);
	    cpmLocation.setLayoutData(textLayout);
	    //CPM Location defaults to preferences if set
	    IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(ClidePreferenceConstants.PREFERENCE_ID);
	    cpmLocation.setText(preferences.get(ClidePreferenceConstants.CPM_LOCATION,  ""));
	    cpmLocation.addModifyListener(modifyListener);
	    Button browseCpm = new Button(parent, SWT.PUSH);
	    browseCpm.setText("Browse");
	    browseCpm.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(parent.getShell());
				//file dialog defaults to workspace location
				fd.setFilterPath(ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString());
				cpmLocation.setText(fd.open());
				wizard.setPageComplete(wizard.isPageComplete());
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				return;
			}
		});
	}

	public String getLocation() {
		return cpmLocation.getText();
	}
}
