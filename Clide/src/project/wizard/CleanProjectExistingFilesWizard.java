package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
/**
 * 
 * @author Clide 2018-2019
 * 
 * This class implements a wizard that allows the user to create a new Clean Project 
 * and corresponding Eclipse project from existing Clean source files. The new project 
 * is opened in the current Eclipse Workspace
 */
public class CleanProjectExistingFilesWizard extends Wizard implements INewWizard {

	CleanProjectWizardExistingPageOne pageone = new CleanProjectWizardExistingPageOne("Page one");
	
	public CleanProjectExistingFilesWizard() {
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
	}

	/**
	 * called on clicking "Finish" in the wizard. Collects all relevant information that the user entered and
	 * calls the ProjectGenerator in order to create the project. If no project name is given by the user
	 * the project name is set to the specified main icl file name.
	 */
	@Override
	public boolean performFinish() {
		String mainIclLocation = pageone.getMainLocation();
		File mainLoc = new File(mainIclLocation);
		String cpmLoc = pageone.getCpmLocation();
		String mainIclName = mainLoc.getName();
		String projectName = pageone.getProjectName();
		if(projectName.equals(""))
			projectName = mainIclName;
		ProjectGenerator.createProjectFromExistingFiles(mainLoc.getParent(), mainIclName, cpmLoc, projectName);
		return true;
	}
	
	public void addPages() {
		addPage(pageone);
	}

}
