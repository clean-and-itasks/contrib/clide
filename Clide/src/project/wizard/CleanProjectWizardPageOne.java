package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import preference.ClidePreferenceConstants;

/**
 * 
 * @author Clide 2018-2019
 * 
 *  First page to be used by "cleanProjectWizard"
 *  Defines the
 * 	layout of the first wizard page in which the user enters mandatory information that
 * 	is needed for project creation.
 *
 */
public class CleanProjectWizardPageOne extends WizardPage {
	
	private Text projectLocation;
	private Text mainIclFilename;
	private ProjectNameSelector projectNameSelector;
		
	private ModifyListener modifyListener = new ModifyListener() {			
		@Override
		public void modifyText(ModifyEvent e) {
			setPageComplete(isPageComplete());
		}
	};
	
	private CPMSelector cpmSelector;
	
	protected CleanProjectWizardPageOne(String pageName) {
		super(pageName);
		setTitle("Clean Project Wizard");
	}

	
	/**
	 * defines the layout of the wizard page. This wizard page includes:
	 * 	- a name to specify a project name
	 * 	- a row to specify the project location (the parent folder in which the new project folder will be created)
	 *  - a row to specify a main ICL file name
	 *  - a row to indicate the location of the CPM to be used for project creation
	 *  changes to any text field calls isPageComplete
	 */
	@Override
	public void createControl(Composite parent) {
		
		//Main Composite, content ordered in a grid
		Composite composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout(3, false);
        // used to set size of text fields
        GridData textLayout = new GridData(WizardConstants.TEXTFIELDWIDTH, WizardConstants.TEXTFIELDHEIGHT);        
        composite.setLayout(layout);
        setControl(composite);

        // Project Name Row
        projectNameSelector = new ProjectNameSelector(composite, textLayout, this);
        
        // Project Location Row
        new Label(composite, SWT.NONE).setText("Project Location");
        projectLocation = new Text(composite, SWT.NONE);
        projectLocation.setLayoutData(textLayout);
        projectLocation.setText(ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString());
        projectLocation.addModifyListener(modifyListener);
        Button browse = new Button(composite, SWT.PUSH);
        browse.setText("Browse");
        browse.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dd = new DirectoryDialog(getShell());				
				//file dialog defaults to workspace location
				dd.setFilterPath(ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString());
				projectLocation.setText(dd.open());
				setPageComplete(isPageComplete());
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				return;
			}
		});

        // ICL Name Row
        new Label(composite, SWT.NONE).setText("Main ICL Filename");
        mainIclFilename = new Text(composite,SWT.NONE);
        mainIclFilename.setLayoutData(textLayout);
        mainIclFilename.addModifyListener(modifyListener);
        // placeholder for grid layout
        new Label(composite, SWT.None).setText("");
         
        
        // CPM Location Row
        cpmSelector = new CPMSelector(composite, textLayout, this);
        
        
        composite.getShell().pack();
	}
	
	/**
	 * determines whether the wizard can be completed.
	 * only allow clicking "finish" if:
	 * - project name is not empty
	 * - project location points to a valid folder
	 * - cpm location points to an existing file
	 * - cpm location points to a file called "cpm"
	 */
	@Override
	public boolean isPageComplete() {
		/* only allow clicking "finish" if:
			- project name is not empty
			- project location points to a valid folder
			- cpm location points to an existing file
			- cpm location points to a file called "cpm"
		*/
		
		
		File location = new File(projectLocation.getText());
		// Check for null because this function seems to be called in the background before the projectNameSelector can be initialized
		if(location.exists() && projectNameSelector != null){
			String name = projectNameSelector.getProjectName();
			return location.isDirectory() && !name.isEmpty() && cpmSelector.checkCPM();
		}
		return false;
	}

	public String getMainIclName() {
		return this.mainIclFilename.getText();
	}
	
	public String getCpmLocation() {
		return cpmSelector.getLocation();
	}
	
	public String getProjectName() {
		return projectNameSelector.getProjectName();
	}
	
	
	public File getProjectLocation() {
		return new File(this.projectLocation.getText());
	}
	
	
}
