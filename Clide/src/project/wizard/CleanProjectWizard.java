package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;
//import java.lang.reflect.Field;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

/**
 * 
 * @author Clide 2018-2019
 * This class implements a wizard that allows the user to create a new Clean Project 
 * and corresponding Eclipse project that is opened in the current Eclipse Workspace
 */
public class CleanProjectWizard extends Wizard implements INewWizard {
	
	private CleanProjectWizardPageOne pageone = new CleanProjectWizardPageOne("Page One");
	private CleanProjectWizardPageTwo pagetwo = new CleanProjectWizardPageTwo("Page Two");
	
	private WizardPage[] pages = new WizardPage[] {pageone, pagetwo};
	
	/**
	 * called on clicking "Finish" in the wizard. Collects all relevant information that the user entered and
	 * calls the ProjectGenerator in order to create the project.
	 */
	@Override
	public boolean performFinish() {
		String projectName = pageone.getProjectName();
		File directory = pageone.getProjectLocation();
		String mainIcl = pageone.getMainIclName();
		String cpmLoc = pageone.getCpmLocation();
		String envLoc = pagetwo.getEnvLocation();
		ProjectGenerator.createProject(projectName, directory, mainIcl, cpmLoc, envLoc);
		return true;
	}

	public void addPages() {
		for(WizardPage p : pages)
			addPage(p);
	}

	
	@Override
	public boolean canFinish() {
		if(pagetwo.getEnvLocation().isEmpty())
			return pageone.isPageComplete();
		return super.canFinish();
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// TODO Auto-generated method stub
		
	}
}
