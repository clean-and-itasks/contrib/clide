package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;

import org.eclipse.core.internal.runtime.Activator;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * 
 * @author Clide 2018-2019
 * 
 * First Page to be used in EclipseFromCleanProjectWizard
 *
 */
public class EclipseProjectWizardPageOne extends WizardPage{

	private Text projectLocation;
	private ProjectNameSelector projectNameSelector;

	protected EclipseProjectWizardPageOne(String pageName) {
		super(pageName);
		setTitle("Clean Project Wizard");
	}

	/**
	 * defines layout of this wizard page. Adds
	 * - a row to choose a project name
	 * - a row to indicate the Clean .prj file
	 */
	@Override
	public void createControl(Composite parent) {
		//Main Composite, content ordered in a grid
		Composite composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout(3, false);
        // used to set size of text fields
        GridData textLayout = new GridData(WizardConstants.TEXTFIELDWIDTH, WizardConstants.TEXTFIELDHEIGHT);        
        composite.setLayout(layout);
        setControl(composite);        
        
        //Project Name Row
        projectNameSelector = new ProjectNameSelector(composite, textLayout, this);
        
        
        // .prj file Location Row
        new Label(composite, SWT.NONE).setText(".prj File Location");
        projectLocation = new Text(composite, SWT.NONE);
        projectLocation.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				setPageComplete(isPageComplete());
				if(projectNameSelector != null && projectNameSelector.getProjectName().equals("")) {
					File prjLoc = new File(projectLocation.getText());
					if(isValidPrjFile(prjLoc)) {
						String prjName = prjLoc.getName();
						prjName = prjName.substring(0, prjName.length()-4);
						projectNameSelector.setProjectName(prjName);
					}
				}
			}
		});

        projectLocation.setLayoutData(textLayout);
        projectLocation.setText(ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString());
        projectLocation.addModifyListener(new ModifyListener(){
	    		@Override
	    		public void modifyText(ModifyEvent e) {
	    			setPageComplete(isPageComplete());			
	    		}
	    	});
        Button browse = new Button(composite, SWT.PUSH);
        browse.setText("Browse");
        browse.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(getShell());	
				String[] allowedExtensions = {"*.prj"};
				fd.setFilterExtensions(allowedExtensions);
				//file dialog defaults to workspace location
				fd.setFilterPath(ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString());
				projectLocation.setText(fd.open());
				setPageComplete(isPageComplete());
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				return;
			}
		});		
      
        composite.getShell().pack();
	}

	/**
	 * assess whether all the relevant data was entered and is valid.
	 * 
	 * @return true if the project file specified in the corresponding text field is valid
	 */
	@Override
	public boolean isPageComplete() {
		/* only allow clicking "finish" if:
			- project location points to a valid .prj file
			- cpm location points to an existing file
			- cpm location points to a file called "cpm"
		*/
		if(projectLocation != null) {
			File prjFile = new File(projectLocation.getText());
			return isValidPrjFile(prjFile);
		}
		return false;
	}
	
	/**
	 * 
	 * @param prjFile a file
	 * @return true if the file exists and has a .prj file ending
	 */
	private boolean isValidPrjFile(File prjFile) {
		if(prjFile.exists())
			return prjFile.getName().substring(prjFile.getName().length()-4).equals(".prj");
		return false;
	}
	
	public File getPrjFile() {
		return new File(this.projectLocation.getText());
	}

	public String getProjectName() {
		return this.projectNameSelector.getProjectName();
	}
}
