package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * 
 * @author Clide 2018-2019
 * 
 *  Second page to be used by "cleanProjectWizard"
 *  Defines the
 * 	layout of the second wizard page in which the user enters mandatory information that
 * 	is needed for project creation.
 *
 */
public class CleanProjectWizardPageTwo extends WizardPage {
	
	Text envLocation;

	protected CleanProjectWizardPageTwo(String pageName) {
		super(pageName);
		setTitle("Clean Project Wizard");
	}
	
	/**
	 * defines the layout of this wizard page, adds a row with label, text field and browse button
	 * that allows the user to select an environment file to be used for project creation.
	 */
	@Override
	public void createControl(Composite parent) {

		// Main Composite, content ordered in a grid
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(3, false);
		// used to set size of text fields
		GridData textLayout = new GridData(WizardConstants.TEXTFIELDWIDTH, WizardConstants.TEXTFIELDHEIGHT);
		composite.setLayout(layout);
		setControl(composite);
		
		
		new Label(composite, SWT.NONE).setText("Environment Location");
        envLocation = new Text(composite, SWT.NONE);
        envLocation.setLayoutData(textLayout);
        envLocation.setText("");
        envLocation.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				setPageComplete(isPageComplete());
			}
		});
        Button browse = new Button(composite, SWT.PUSH);
        browse.setText("Browse");
        browse.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				FileDialog fd = new FileDialog(getShell());
				fd.setText("Select environment file");
				envLocation.setText( fd.open());
				setPageComplete(isPageComplete());
			}
		});
        
        composite.getShell().pack();
	}

	public boolean isPageComplete() {
		return (new File(envLocation.getText())).isFile();
	}
	
	public String getEnvLocation() {
		return envLocation.getText().toString();
	}

}
