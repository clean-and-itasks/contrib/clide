package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * 
 * @author Clide 2018-2019
 *
 * Defines a user interface to be used in the project wizards. Instantiating this class creates a row
 * in an SWT user interface with a label and text field that lets the user select the name
 * used for clean project creation.
 */
public class ProjectNameSelector {
	
	private Text projectName;
	private WizardPage wizard;
	
	private ModifyListener modifyListener = new ModifyListener(){
		@Override
		public void modifyText(ModifyEvent e) {
			wizard.setPageComplete(wizard.isPageComplete());
		}
	};
	
	/**
	 * 
	 * @param parent		the parent composite
	 * @param textLayout	the layout to be used for the text field
	 * @param wizard		the wizard page that the project name selector is added to
	 */
	public ProjectNameSelector(Composite parent, GridData textLayout, WizardPage wizard) {
		this.wizard = wizard;
	
        new Label(parent, SWT.NONE).setText("Project Name");
        projectName = new Text(parent,SWT.NONE);
        projectName.setLayoutData(textLayout);
        projectName.addModifyListener(modifyListener);
        //placeholder for grid layout
        new Label(parent, SWT.NONE).setText("");
	}

	public String getProjectName() {
		return this.projectName.getText();
	}

	public void setProjectName(String prjName) {
		projectName.setText(prjName);
	}
	
}
