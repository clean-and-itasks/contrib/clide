package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;

import console.ColorConstants;
import console.ConsoleInterfacer;
import services.ExternalCommandService;

/**
 * 
 * @author Clide 2018-2019
 * 
 * Utility class that handles creation of Clean and Eclipse Projects
 *
 */
public class ProjectGenerator {

	/**
	 * Creates a new folder and creates a valid Clean project in it.
	 * After the Clean project is created, creates an eclipse project that includes this Clean project and
	 * opens it in the current eclipse workspace
	 * 
	 * @param projectName      a project name
	 * @param directory        the directory in which the project is created
	 * @param mainIcl          the name of the .icl file that contains the Start function of the Clean program
	 * @param cpmLoc           the path to the CPM to be used to create the clean project
	 * @param envLoc           the path to the environment file to be used for this Clean project
	 */
	public static void createProject(String projectName, File directory, String mainIcl, String cpmLoc, String envLoc) {

		// if no name is given for the icl file default to the project name
		if (mainIcl.equals("") || mainIcl == null)
			mainIcl = projectName + ".icl";
		else
			mainIcl = mainIcl.split("\\.")[0] + ".icl";

		// create Project Directory
		Path projectPath = Paths.get(directory.toString(), projectName);
		File projectFilePath = projectPath.toFile();
		System.out.println(projectFilePath.toString());
		projectFilePath.mkdir();

		File iclFile = new File(projectFilePath.getAbsolutePath() + "/" + mainIcl);
		try {
			// create "Hello World" icl file as main project file
			iclFile.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(iclFile));
			String header = "module " + mainIcl.split("\\.")[0] + "\n\n";
			String start = "Start :: String\nStart = \"Hello World!\"";
			String content = header + start;
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//create .prj file
		createPrjFile(cpmLoc, mainIcl, projectFilePath);
		
		

		ExternalCommandService ecs = ExternalCommandService.getInstance();

		// create .prj file
		List<String> command = Arrays.asList(cpmLoc, "project", mainIcl, "create");
		try {
			ecs.runExternalCommand(command, projectFilePath.toString());
		} catch (IOException e1) {
			ConsoleInterfacer.writeToConsole("CLIDE-std",
					ColorConstants.sRed("IOException: please provide a correct command"));
			e1.printStackTrace();
		}

		// set environment path
		if (!envLoc.isEmpty())
			try {
				ecs.runExternalCommand(Arrays.asList(cpmLoc, "project", projectName, "target", envLoc),
						projectFilePath.toString());
			} catch (IOException e1) {
				ConsoleInterfacer.writeToConsole("CLIDE-std",
						ColorConstants.sRed("IOException: please provide a correct command"));
				e1.printStackTrace();
			}

		// Create Eclipse Project from Project Folder
		createEclipseProject(projectName, projectFilePath);
		
	}

	/**
	 * Creates a Clean project with existing source files
	 * 
	 * @param projectLocation		the path to the root folder of the project
	 * @param mainIclFilename		the filename of the .icl file that includes the Start function
	 * 								for the Clean project
	 * @param cpmLoc                path to the CPM to be used to create the Clean project
	 * @param projectName			the name to be used for the Eclpise project
	 */
	public static void createProjectFromExistingFiles(String projectLocation, String mainIclFilename, String cpmLoc, String projectName) {
		File projLoc = new File(projectLocation); 
		createPrjFile(cpmLoc, mainIclFilename, projLoc);
		createEclipseProject(projectName, projLoc);
	}
	
	/**
	 * Calls the CPM in order to create a new Clean project
	 * 
	 * @param cpmLoc		the location of the CPM to be used for project creation
	 * @param mainIcl		the location of the .icl file that includes the Start function for this Clean project
	 * @param projectLoc    the location of the root folder in which the project file is to be created
	 */
	private static void createPrjFile(String cpmLoc, String mainIcl, File projectLoc) {
		ExternalCommandService ecs = ExternalCommandService.getInstance();
		List<String> command = Arrays.asList(cpmLoc, "project", mainIcl, "create");
		try {
			ecs.runExternalCommand(command, projectLoc.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * creates an eclipse project at the specified location and opens it in the current workspace
	 * @param projectName		the name to be used as the eclipse project name
	 * @param projectFilePath	the path to the project folder
	 */
	public static void createEclipseProject(String projectName, File projectFilePath) {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IProject project = workspace.getRoot().getProject(projectName);
		IProjectDescription description = workspace.newProjectDescription(project.getName());
		description.setLocationURI(projectFilePath.toURI());
		try {
			// Project is created and opened here
			project.create(description, new NullProgressMonitor());
			project.open(new NullProgressMonitor());
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
}


