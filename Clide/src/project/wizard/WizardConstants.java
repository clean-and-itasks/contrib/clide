package project.wizard;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/

/**
 * @author Clide 2018-2019
 * 
 * Constants for use in project wizards
 */
public final class WizardConstants {
	
	private WizardConstants() {};
	
	public static final int TEXTFIELDWIDTH = 400;
	public static final int TEXTFIELDHEIGHT = 20;

}
