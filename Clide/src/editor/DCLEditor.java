package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.ui.editors.text.TextEditor;
/**
 * Editor for viewing .dcl files
 * @author clide 2018-19
 *
 */
public class DCLEditor extends TextEditor {

	private ColorManager colorManager;

	/**
	 * Set the source viewer configuration and document provider.
	 */
	public DCLEditor() {
		super();
		colorManager = new ColorManager();
		setSourceViewerConfiguration(new CleanConfiguration(colorManager));
		setDocumentProvider(new CleanTextDocumentProvider());
	}

	/**
	 * Dispose.
	 */
	@Override
	public void dispose() {
		colorManager.dispose();
		super.dispose();
	}
}
