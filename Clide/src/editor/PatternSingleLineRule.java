package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
/**
 * Rule for highlighting Clean functions.
 * @author clide 2018-19
 *
 */
public class PatternSingleLineRule implements IRule {

	IToken token;
	Pattern pattern;

	public PatternSingleLineRule(IToken token, String pattern) {
		this.token = token;
		this.pattern = Pattern.compile(pattern);
	}

	/**
	 * Evaluate whether the scanner contains the desired pattern.
	 */
	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		String stream = "";
		int c = scanner.read();
		int count = 1;

		// Only attempt matching if scanner started in 0th column. To prevent non functions being highlighted.
		if (scanner.getColumn() == 1)
			while (c != ICharacterScanner.EOF) {
				stream += (char) c;
				count++;
				c = scanner.read();
				String follows = checkNextTwoChar(scanner);
				// If there's a newline following, or a comment, match now and reset the stream.
				if (c == '\n' || c == '\r' || follows.equals("//")) {
					Matcher m = pattern.matcher(stream);
					if (m.matches()) {
						return token;
					}
					stream = "";
				}
			}
		// Match once more after reading the whole stream. This is for the highlighting
		// to work while writing in-line (since there's no newline character read then).
		Matcher m = pattern.matcher(stream);
		if (m.matches()) {
			return token;
		}
		for (int i = 0; i < count; i++) {
			scanner.unread();
		}
		return Token.UNDEFINED;
	}

	/**
	 * Pseudo-read the next two characters in the scanner. This is to check whether there's a comment in-line with a function header.
	 * @param scanner
	 * @return the next two characters
	 */
	private String checkNextTwoChar(ICharacterScanner scanner) {
		int nextC = scanner.read();
		int secNextC = scanner.read();
		scanner.unread();
		scanner.unread();
		String follows = "" + (char) nextC + (char) secNextC;
		return follows;
	}
}
