package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.jface.text.rules.IWhitespaceDetector;
/**
 * Generic whitespace detector.
 * 
 * @author clide 2018-19
 */
public class CleanWhiteSpaceDetector implements IWhitespaceDetector {

	@Override
	public boolean isWhitespace(char c) {
		return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
	}

}
