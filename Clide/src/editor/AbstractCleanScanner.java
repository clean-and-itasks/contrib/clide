package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.util.List;

import org.eclipse.jface.text.rules.*;
/**
 * Abstract scanner that includes all keywords to be highlighted and the function for adding them. 
 * Used to reduce duplicate code with CleanCodeScanner and CleanTypeScanner
 * @author clide 2018-19
 *
 */
public abstract class AbstractCleanScanner extends RuleBasedScanner{
	private static String[] conditional = { "if", "case" };
	private static String[] label = { "let!", "with", "where", "in", "of" };
	private static String[] keywords = { "infix", "infixr", "infix" };
	private static String[] moduleSystem = { "module", "implementation", "definition", "system" };
	private static String[] typeClass = { "class", "instance", "export" }; 
	private static String[] includeKeyword = { "from", "import", "as", "qualified" };
	private static String[] special = {"Start"};
	private static String[] boolDenot = {"True","False"};
	// private static String[] basicType = {"Int","Real","Char","Bool","String"};
	// private static String[] specialType = {"World","ProcId","Void","Files","File"};

	/**
	 * Add all keywords to the word detection.
	 * @param defaultT
	 * @param keyword
	 * @param rules
	 */
	protected void addWordRules(IToken defaultT, IToken keyword, List<IRule> rules) {
		WordRule wordRule = new WordRule(new IWordDetector() {
			public boolean isWordStart(char c) {
				return Character.isJavaIdentifierStart(c);
			}

			public boolean isWordPart(char c) {
				return Character.isJavaIdentifierPart(c);
			}
		}, defaultT);
		for (String word : conditional) {
			wordRule.addWord(word, keyword);
		}
		for (String word : label) {
			wordRule.addWord(word, keyword);
		}
		for (String word : typeClass) {
			wordRule.addWord(word, keyword);
		}
		for (String word : includeKeyword) {
			wordRule.addWord(word, keyword);
		}
		for (String word : moduleSystem) {
			wordRule.addWord(word, keyword);
		}
		for (String word : keywords) {
			wordRule.addWord(word, keyword);
		}
		for (String word : special) {
			wordRule.addWord(word, keyword);
		}
		for (String word : boolDenot) {
			wordRule.addWord(word, keyword);
		}
		rules.add(wordRule);
	}
}
