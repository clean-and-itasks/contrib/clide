package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.rules.*;

/*
 * Source: https://github.com/eclipse/eclipse.platform.text
 */
/**
 * Scanner that splits document into partitions as specified. 
 * 
 * @author clide 2018-19
 *
 */
public class CleanPartitionScanner extends RuleBasedPartitionScanner {
	public final static String CLEAN_MULTI_COMMENT = "__clean_multi_comment";
	public final static String CLEAN_MULTI_TYPE = "__clean_multi_type";
	public final static String[] CLEAN_PARTITION_TYPES= new String[] {CLEAN_MULTI_COMMENT, CLEAN_MULTI_TYPE};
	
	/**
	 * Add rules for document partitioning. 
	 */
	public CleanPartitionScanner() {
		IToken cleanComment = new Token(CLEAN_MULTI_COMMENT);
		IToken cleanType = new Token(CLEAN_MULTI_TYPE);

		List<IPredicateRule> rules = new ArrayList<>();

		// Add rule for strings and character constants.
		rules.add(new SingleLineRule("\"", "\"", Token.UNDEFINED, '\\')); 
		rules.add(new SingleLineRule("'", "'", Token.UNDEFINED, '\\')); 
	
		// Add special case word rule for empty multi-line comments
		rules.add(new WordPredicateRule(cleanComment));
		
		// Add rule for multi-line comments.
		rules.add(new MultiLineRule("/*", "*/", cleanComment));
		
		// Add rule for multi-line types
		rules.add(new PatternMultiLineRule(cleanType, "[ \t]*::"));
		
		IPredicateRule[] result = new IPredicateRule[rules.size()];
		rules.toArray(result);
		setPredicateRules(result);
	}
	
	/**
	 * Detector for empty comments.
	 */
	static class EmptyCommentDetector implements IWordDetector {

		@Override
		public boolean isWordStart(char c) {
			return (c == '/');
		}

		@Override
		public boolean isWordPart(char c) {
			return (c == '*' || c == '/');
		}
	}

	/**
	 * A rule that scans for empty comments.
	 */
	static class WordPredicateRule extends WordRule implements IPredicateRule {

		private IToken fSuccessToken;

		public WordPredicateRule(IToken successToken) {
			super(new EmptyCommentDetector());
			fSuccessToken= successToken;
			addWord("/**/", fSuccessToken); 
		}

		@Override
		public IToken evaluate(ICharacterScanner scanner, boolean resume) {
			return super.evaluate(scanner);
		}

		@Override
		public IToken getSuccessToken() {
			return fSuccessToken;
		}
	}

}
