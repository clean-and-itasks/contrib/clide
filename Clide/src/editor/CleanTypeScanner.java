package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.*;
/**
 * Scanner for type clean code.
 * @author clide 2018-19
 *
 */
public class CleanTypeScanner extends AbstractCleanScanner{

	/**
	 * Add rules for all desired highlighting. 
	 * @param colorManager
	 */
	public CleanTypeScanner(ColorManager colorManager) {
		IToken defaultT = new Token(new TextAttribute(colorManager.getColor(CleanColorConstants.TYPE)));
		IToken string = new Token(new TextAttribute(colorManager.getColor(CleanColorConstants.STRING)));
		IToken charT = new Token(new TextAttribute(colorManager.getColor(CleanColorConstants.CHAR)));
		IToken comment = new Token(new TextAttribute(colorManager.getColor(CleanColorConstants.COMMENT)));
		IToken keyword = new Token(new TextAttribute(colorManager.getColor(CleanColorConstants.KEYWORD)));

		List<IRule> rules = new ArrayList<>();
		
		// Add rule for strings, chars and 1-line comments
		rules.add(new SingleLineRule("\"", "\"", string, '\\'));
		rules.add(new SingleLineRule("'", "'", charT, '\\'));
		rules.add(new EndOfLineRule("//", comment));
		
		// Add rules for keywords
		addWordRules(defaultT, keyword, rules);

		// Add generic whitespace rule
		rules.add(new WhitespaceRule(new CleanWhiteSpaceDetector()));

		IRule[] result = new IRule[rules.size()];
		rules.toArray(result);
		setRules(result);
	}
}
