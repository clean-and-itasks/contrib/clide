package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
/**
 * Takes care of color managing for highlighting.
 * @author clide 2018-19
 *
 */
public class ColorManager {

	protected Map<RGB, Color> fColorTable = new HashMap<>(10);

	/**
	 * Dispose of colors.
	 */
	public void dispose() {
		fColorTable.values().forEach(Color::dispose);
	}
	
	/**
	 * Return the color corresponding to the RGB value.
	 * @param rgb
	 * @return color
	 */
	public Color getColor(RGB rgb) {
		Color color = fColorTable.get(rgb);
		if (color == null) {
			color = new Color(Display.getCurrent(), rgb);
			fColorTable.put(rgb, color);
		}
		return color;
	}
}
