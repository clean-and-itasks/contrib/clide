package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.swt.graphics.RGB;
/**
 * The color constants used for syntax highlighting. 
 * 
 * @author clide 2018-19
 */
public class CleanColorConstants {
	public static final RGB DEFAULT = new RGB(0,0,0);
	public static final RGB STRING = new RGB(50,205,50);
	public static final RGB CHAR = new RGB(199,21,133);
	public static final RGB COMMENT = new RGB(0,0,240);
	public static final RGB MULTI_LINE_COMMENT = new RGB(0,0,240);
	public static final RGB FUNCTION = new RGB(220,20,60);
	public static final RGB TYPE = new RGB(255,165,0); // In the CleanIDE this is the same as function. We choose to color types differently.
	public static final RGB KEYWORD = new RGB(138,43,226);
}
