package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.ui.editors.text.TextEditor;
/**
 * Editor for viewing .icl files
 * @author clide 2018-19
 *
 */
public class ICLEditor extends TextEditor {
	/*
	 * Sources: 
	 * Eclipse XML editor plugin example.
	 * https://stackoverflow.com/questions/455210/eclipse-editor-plugin-error-when-opening-file-outside-project
	 */
	
	private ColorManager colorManager;
	
	/**
	 * Set the source viewer configuration and document provider.
	 */
	public ICLEditor() {
		super();
		colorManager = new ColorManager();
		setSourceViewerConfiguration(new CleanConfiguration(colorManager));
		setDocumentProvider(new CleanTextDocumentProvider());
	}
	
	/**
	 * Dispose.
	 */
	@Override
	public void dispose() {
		colorManager.dispose();
		super.dispose();
	}
}
