package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
/**
 * Rule for highlighting Clean types over multiple lines.
 * @author clide 2018-19
 *
 */
public class PatternMultiLineRule implements IPredicateRule {

	IToken token;
	Pattern startSequence;

	public PatternMultiLineRule(IToken token, String startSequence) {
		this.token = token;
		this.startSequence = Pattern.compile(startSequence);
	}
	
	/**
	 * Call evaluate function with resume set to false.
	 */
	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		return evaluate(scanner, false);
	}

	/**
	 * Evaluate whether the scanner contains the desired pattern.
	 */
	@Override
	public IToken evaluate(ICharacterScanner scanner, boolean resume) {
		String stream = "";
		int c = scanner.read();
		int count = 1;
		boolean startFound = false;
		
		// Only attempt match if you started reading from the 0th column. To prevent functions
		// being read as types.
		if (scanner.getColumn() == 1)
			while (c != ICharacterScanner.EOF) {
				stream += (char) c;
				count++;
				c = scanner.read();

				// Check for start sequence
				startFound = matchStartSeq(stream, startFound);

				// Check for type end:
				// Either: EOF, or, no tab/space after new line
				if (startFound && (scanner.getColumn() == 0 || c == ICharacterScanner.EOF)) {
					int nextC = nextC(scanner);
					if (nextC != ' ' && nextC != '\t') {
						return token;
					}
				}
				stream = resetStream(stream, c, startFound);
			}
		resetScanner(scanner, count);
		return Token.UNDEFINED;
	}
	/**
	 * Rewind scanner if there was no match.
	 * @param scanner
	 * @param count
	 */
	private void resetScanner(ICharacterScanner scanner, int count) {
		for (int i = 0; i < count; i++) {
			scanner.unread();
		}
	}
	/**
	 * If no start has been found, reset the stream after each new line. To prevent huge strings & time complexity.
	 * @param stream
	 * @param c
	 * @param startFound
	 * @return stream
	 */
	private String resetStream(String stream, int c, boolean startFound) {
		if (!startFound && (c == '\n' || c == '\r'))
			stream = "";
		return stream;
	}
	
	/**
	 * Match the current stream to the start sequence. If it matches, remember it in startFound.
	 * @param stream
	 * @param startFound
	 * @return whether start was found
	 */
	private boolean matchStartSeq(String stream, boolean startFound) {
		Matcher startM = startSequence.matcher(stream);
		if (startM.matches()) {
			startFound = true;
		}
		return startFound;
	}

	/**
	 * Pseudo reads the next character in the file, to look at it ahead of time.
	 * @param scanner
	 * @return next character
	 */
	private int nextC(ICharacterScanner scanner) {
		int nextC = scanner.read();
		scanner.unread();
		return nextC;
	}

	/**
	 * Set success token.
	 */
	@Override
	public IToken getSuccessToken() {
		return token;
	}

}
