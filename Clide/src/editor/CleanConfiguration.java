package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.BufferedRuleBasedScanner;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
/*
 * Sources: 
 * Eclipse XML plugin template; 
 * Eclipse github: https://github.com/eclipse/eclipse.platform.text
 */
/**
 * Class responsible for configuring how the source (document) is viewed in the editor. Sets up partition scanner and scanners for respective partitions.
 * @author clide 2018-19
 *
 */
public class CleanConfiguration extends SourceViewerConfiguration {
	
	private ColorManager colorManager;
	private CleanCodeScanner scanner;
	private CleanTypeScanner typeScanner;

	public CleanConfiguration(ColorManager colorManager) {
		this.colorManager = colorManager;
	}
	
	/**
	 * Specify content types of the document.
	 */
	@Override
	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
		return new String[] { 
				IDocument.DEFAULT_CONTENT_TYPE, 
				CleanPartitionScanner.CLEAN_MULTI_COMMENT,
				CleanPartitionScanner.CLEAN_MULTI_TYPE
				};
	}
	
	/**
	 * Initialize and return the scanner for clean code.
	 * @return clean scanner
	 */
	protected CleanCodeScanner getCleanScanner() {
		if (scanner == null) {
			scanner = new CleanCodeScanner(colorManager);
			scanner.setDefaultReturnToken(
					new Token(new TextAttribute(colorManager.getColor(CleanColorConstants.DEFAULT))));
		}
		return scanner;
	}
	
	/**
	 * Initialize and return the scanner for code within types.
	 * @return type scanner
	 */
	protected CleanTypeScanner getCleanTypeScanner() {
		if (typeScanner == null) {
			typeScanner = new CleanTypeScanner(colorManager);
			typeScanner.setDefaultReturnToken(
					new Token(new TextAttribute(colorManager.getColor(CleanColorConstants.TYPE))));
		}
		return typeScanner;
	}
	/**
	 * Set the damager and repairer for each content type (default, comment, type).
	 */
	@Override
	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {
		PresentationReconciler reconciler = new PresentationReconciler();
		
		DefaultDamagerRepairer dr = new DefaultDamagerRepairer(getCleanScanner());
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

		dr = new DefaultDamagerRepairer(new SingleTokenScanner(
				new TextAttribute(colorManager.getColor(CleanColorConstants.MULTI_LINE_COMMENT))));
		reconciler.setDamager(dr, CleanPartitionScanner.CLEAN_MULTI_COMMENT);
		reconciler.setRepairer(dr, CleanPartitionScanner.CLEAN_MULTI_COMMENT);
		
		dr = new DefaultDamagerRepairer(getCleanTypeScanner());
		reconciler.setDamager(dr, CleanPartitionScanner.CLEAN_MULTI_TYPE);
		reconciler.setRepairer(dr, CleanPartitionScanner.CLEAN_MULTI_TYPE);

		return reconciler;
	}
	
	/**
	 * Scanner that scans for single token.
	 */
	static class SingleTokenScanner extends BufferedRuleBasedScanner {
		public SingleTokenScanner(TextAttribute attribute) {
			setDefaultReturnToken(new Token(attribute));
		}
	}
}
