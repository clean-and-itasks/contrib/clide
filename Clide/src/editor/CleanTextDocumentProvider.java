package editor;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.rules.FastPartitioner;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;
/**
 * Responsible for providing the document for the editor and setting the partitions.
 * 
 * @author clide 2018-19
 *
 */
public class CleanTextDocumentProvider extends TextFileDocumentProvider {
	/*
	 * Source: https://stackoverflow.com/questions/455210/eclipse-editor-plugin-error-when-opening-file-outside-project
	 */
	/**
	 * Initialize the partition scanner and connect it to the document.
	 */
	@Override
	protected FileInfo createFileInfo(Object element) throws CoreException {
        FileInfo info = super.createFileInfo(element);
        if(info==null){
            info = createEmptyFileInfo();
        }
        IDocument document = info.fTextFileBuffer.getDocument();
        if (document != null) {
        	IDocumentPartitioner partitioner = new FastPartitioner(new CleanPartitionScanner(),
					CleanPartitionScanner.CLEAN_PARTITION_TYPES);
			partitioner.connect(document);
			document.setDocumentPartitioner(partitioner);
        }
        return info;
    }


}
