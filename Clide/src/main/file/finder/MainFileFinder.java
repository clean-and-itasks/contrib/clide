package main.file.finder;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

import console.ConsoleInterfacer;

/**
 * This class is responsible for finding the main .icl file, i.e. the file containing the Start function, for a given project
 * 
 * @author CLIDE 2018-2019
 */

public class MainFileFinder {
	
	/**
	 * finds all files with a Start function for a given project
	 * @param project: the eclipse project for which the main file should be found
	 * @return array of files that contain a Start function
	 */

	public static IFile[] getMainFiles(IProject project) {
		
		// check if project actually exists
		if(!project.exists())
			return new IFile[0];

		MainFileVisitor visitor = new MainFileVisitor();
		
		try {
			project.accept(visitor);
		} catch (CoreException e) {
			System.out.println("Project did not accept Main File Visitor");
			e.printStackTrace();
			ConsoleInterfacer.writeToConsole("CLIDE-std", "Could not automatically determine main file. Please try again!");
		}
		
		return visitor.getFiles();

	}
	
}