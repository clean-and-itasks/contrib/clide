package main.file.finder;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;

import console.ConsoleInterfacer;

/**
 * The visitor class that checks a IResource objects and determines whether it contains a Start 
 * function or whether the search should be aborted/continued
 * 
 * @author CLIDE 2018-2019
 */

public class MainFileVisitor implements IResourceVisitor {
	
	private List<IFile> mainFiles = new ArrayList<>();


	/**
	 * @see IResourceVisitor#visit(IResource)
	 */
	@Override
	public boolean visit(IResource resource) {

		switch (resource.getType()) {
		case IResource.FILE:
			if (resource.getName().endsWith(".icl"))
				try {
					searchFile((IFile) resource);
				} catch (CoreException e) {
					System.out.println("Could not open file");
					e.printStackTrace();
				}
			return false;
			
		case IResource.PROJECT:
			return true;
			
		case IResource.FOLDER:
			return true;
			
		default:
			return false;
		}

	}

	/**
	 * Searches a file and checks if it contains a Start function. Adds correct files to mainFiles field.
	 * @param file: the file that should be searched
	 * @throws CoreException
	 */
	private void searchFile(IFile file) throws CoreException {
		
		// check if file exists
		if(!file.exists())
			return;
		
		InputStream in = file.getContents();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));

		String line;
		try {
			while ((line = reader.readLine()) != null)
				if (line.startsWith("Start")) {
					mainFiles.add(file);
					return;
				}
		} catch (IOException e) {
			System.out.println("Clould not read file " + file.getName());
			e.printStackTrace();
		} finally {
			// always try to close reader which closes underlying input stream
			// prevents resource leakage 
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * get all files containing a Start function
	 * @return array of all files containing a Start function
	 */
	public IFile[] getFiles() {
		return mainFiles.toArray(new IFile[mainFiles.size()]);
	}

}
