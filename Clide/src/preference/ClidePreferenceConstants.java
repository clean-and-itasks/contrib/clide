package preference;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/

/**
 * Interface for keeping the Preference Constants.
 * Should eventually be replaced by final class with static attributes as this is bad style.
 * 
 * @author CLIDE 2018-2019
 */
public interface ClidePreferenceConstants {

	String PREFERENCE_ID = "Clide.preferences";
	
	String CPM_LOCATION = "CPM Location";
	
	String STACK_SIZE = "Stack Size";
	
	String HEAP_SIZE = "Heap Size";

}
