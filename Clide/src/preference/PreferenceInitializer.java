package preference;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;

/**
 * Class responsible for initializing the clide preferences.
 * Used for setting the default/fallback values.
 * 
 * @author CLIDE 2018-2019
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {
	

	public PreferenceInitializer() {
		super();
	}

	/**
	 * @see AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IEclipsePreferences node = DefaultScope.INSTANCE.getNode("clide.preferences");
		
		node.put(ClidePreferenceConstants.STACK_SIZE, "200M");
		node.put(ClidePreferenceConstants.HEAP_SIZE, "2000M");
	}

}
