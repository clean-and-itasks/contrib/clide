package services;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;

import console.ConsoleInterfacer;


/*
 * Singleton class that is able to run external commands in a thread safe manner. 
 *
 * @author Clide 2018-2019
 */
public class ExternalCommandService {


	private String commandOutput;
	private String errorMessage;
	private static ExternalCommandService instance;
	
	/**
	 * Gets singleton in a thread safe manner (with lazy initialization)
	 * @return singleton instance
	 */
	public static synchronized ExternalCommandService getInstance() {
		// lazy singleton pattern
		if(instance == null) {
			instance = new ExternalCommandService();
		}
		return instance;
	}
	
	
	/**
	 * Function for calling the runExternalCommand without having to specify a working directory
	 * @param command a List-type whose elements form a command to be run in your OS's command prompt.
	 * @return String Array with command output at the first index, and potential errors at second index
	 * @throws IOException
	 */
	public synchronized String[] runExternalCommand(List<String> command) throws IOException {
		return runExternalCommand(command, null);
	}
	
	
	/**
 	 * Function for calling the runExternalCommand without having to specify the 'inputLines' parameter for interactive mode
	 * @param command a List-type whose elements form a command to be run in your OS's command prompt
	 * @return String Array with command output at the first index, and potential errors at second index
	 * @throws IOException
	 */
	public synchronized String[] runExternalCommand(List<String> command, String workingDirectory) throws IOException {
		return runExternalCommand(command, workingDirectory, 0);
	}
	

	/**
	 * Runs external command and returns list of results
	 * @param command a List-type whose elements form a command to be run in your OS's command prompt.
	 * @param workingDirectory the working directory on your system from where the command will be run
	 * @param inputLines indicates how many lines to read from the user (option required when run in 'interactive mode')
	 * @return String Array with command output at the first index, and potential errors at second index
	 * @throws IOException 
	 */
	public synchronized String[] runExternalCommand(List<String> command, String workingDirectory, int inputLines) throws IOException {
		//reset command output and error output
		commandOutput = "";
		errorMessage = "";
		
		ProcessBuilder pb = new ProcessBuilder(command);
		if(workingDirectory != null)
			pb.directory(new File(workingDirectory));
		Process process = pb.start();
		
		// Create a print writer that writes to stdint of the process
		// This is needed because the clean .exe on Windows expects some sort of input to fully terminate
		PrintWriter pw = new PrintWriter(process.getOutputStream() );
		
		// if interactive is true, then this reads as many lines of input
		// from the console as specified in 'inputLines'
		if(inputLines > 0) {
			String[] input = ConsoleInterfacer.readFromConsole("CLIDE-Input-Console", inputLines);
			for(String line : input)
				pw.println(line);
		}
		
		pw.println("This print is necessary to ensure proper functionality on windows. "
				+ "If you see this message, that probably means that your program expects user input "
				+ "that you did not provide yourself. Try running the program interactively "
				+ "(not yet implemented!)");
		
		// it is necessary to flush the writer
		pw.flush();
		// Reads output of the external command
		BufferedReader outputBuffer = new BufferedReader(
				new InputStreamReader(process.getInputStream()));
		commandOutput = outputBuffer.readLine();
		String outputLine;

		// Be aware: commands such as 'ls' do not preserve spaces between results
		while ((outputLine = outputBuffer.readLine()) != null) 
			commandOutput += outputLine;
		
		// Reads error messages of the external command
		BufferedReader errorBuffer = new BufferedReader(
				new InputStreamReader(process.getErrorStream()));
		
		String errorLine;
		while ((errorLine = errorBuffer.readLine()) != null) 
			errorMessage += errorLine;
		
	String[] results = {commandOutput, errorMessage};
	return results;
}
}
	
