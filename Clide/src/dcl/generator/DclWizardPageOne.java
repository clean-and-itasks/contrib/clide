package dcl.generator;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.util.ArrayList;

import org.eclipse.jface.resource.ImageDescriptor;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

/**
 * Page used by the DclWizard, includes a scrolling list view
 * that shows all imports and definition statements that can be
 * added to the .dcl file. Each element can be selected by the user
 * by clicking a checkbox next to the element
 * 
 * @author CLIDE 2018-2019
 *
 */
public class DclWizardPageOne extends WizardPage {

	private ArrayList<String> imports;
	private ArrayList<LabelWithCheckbox> importListItems = new ArrayList<>();
	
	private ArrayList<LabelWithCheckbox> definitionListItems = new ArrayList<>();
	private ArrayList<String> definitions;
	
	/**
	 * set title of this wizard page
	 * 
	 * @param pageName			the page name to be displayed to the user
	 */
	private DclWizardPageOne(String pageName) {
		super(pageName);
		setTitle("Choose what to add to the .dcl file");
	}
	
	/**
	 * @param pageName			the page name to be displayed to the user
	 * @param imports			list of imports to be included in the list of selectable elements
	 * @param definitions		list of definitions to be included in the list of selectable elements
	 */
	public DclWizardPageOne(String pageName, ArrayList<String> imports, ArrayList<String> definitions) {
		this(pageName);
		this.imports = imports;
		this.definitions = definitions;
	}

	/**
	 * specifies the layout and content of this wizard page
	 * the page consists of a scrollable list of labels with checkboxes that
	 * list all imports and definitions that can be included in the .dcl file
	 */
	public void createControl(Composite parent) {
		setControl(parent);

	    ScrolledComposite sc = new ScrolledComposite(parent, SWT.V_SCROLL | SWT.H_SCROLL);

	    Composite child = new Composite(sc, SWT.NONE);
	    child.setLayout(new FillLayout());
	    child.setSize(child.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	    Composite list = new Composite(child, SWT.None);
	    list.setLayout(new GridLayout(1, false));
	    for(String text : imports) {
        	LabelWithCheckbox element = new LabelWithCheckbox(text, true, list);
        	importListItems.add(element);
	    }
	    
	    for(String text : definitions) {
        	LabelWithCheckbox element = new LabelWithCheckbox(text, true, list);
        	definitionListItems.add(element);
	    }

	    
	    sc.setContent(child);
	    sc.setMinSize(list.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	    sc.setExpandHorizontal(true);
	    sc.setExpandVertical(true);
	}
	
	/**
	 * loops over list of labels with checkboxes for import statements and gets the selected statements
	 * 
	 * @return			list of selected imports as strings
	 */
	public ArrayList<String> getSelectedImports() {
		ArrayList<String> selectedStrings = new ArrayList<>();
		for(LabelWithCheckbox e : importListItems) 
			if(e.isSelected())
				selectedStrings.add(e.getText());
		
		return selectedStrings;
	}

	/**
	 * loops over list of labels with checkboxes for definition statements and gets the selected statements
	 * 
	 * @return			list of selected definitions as strings
	 */
	public ArrayList<String> getSelectedDefinitions() {
		ArrayList<String> selectedStrings = new ArrayList<>();
		for(LabelWithCheckbox e : definitionListItems) 
			if(e.isSelected())
				selectedStrings.add(e.getText());
		
		return selectedStrings;
	}
	
}
