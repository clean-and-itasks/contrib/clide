package dcl.generator;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Composite;

/**
 * Wizard for creation of .dcl files
 * @author CLIDE 2018-2019
 *
 */
public class DclWizard extends Wizard {
	
	/**
	 * the module name
	 */
	private String module;
	/**
	 * a list of import statements that can be added to the .dcl file
	 */
	private ArrayList<String> imports;
	/**
	 * a list of definition statements that can be added to the .dcl file
	 */
	private ArrayList<String> definitions;
	/**
	 * the wizard page that is presented to the user
	 */
	private DclWizardPageOne pageone;
	/**
	 * directory in which the .dcl file will be created
	 */
	private File directory;
	
	/**
	 * Initializes fields
	 * 
	 * @param directory			directory in which .dcl will be created
	 * @param module			module name for .dcl file
	 * @param imports			list of import statements
	 * @param definitions		list of definition statements
	 */
	public DclWizard(File directory, String module, ArrayList<String> imports, ArrayList<String> definitions) {
		this.module = module;
		this.imports = imports;
		this.definitions = definitions;
		this.pageone = new DclWizardPageOne("Page One", imports, definitions);
		this.directory = directory;
	}

	/**
	 * add page to the Wizard
	 */
	@Override
	public void addPages() {
		addPage(pageone);
	}

	/**
	 * called when the "Finish" button of the Wizard is called
	 * retrieves the imports and definition statements that were selected
	 * and should be included in the new .dcl file and initiates .dcl generation with this information
	 */
	@Override
	public boolean performFinish() {
		ArrayList<String> imports = pageone.getSelectedImports();
		ArrayList<String> definitions = pageone.getSelectedDefinitions();
		try {
			DclGenerator.createFile(directory, this.module, imports, definitions);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
}
