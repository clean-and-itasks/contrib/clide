package dcl.generator;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * 
 * @author CLIDE 2018-2019
 * This class combines an SWT checkbox with a text Label
 * The elements are laid out in a row
 */
public class LabelWithCheckbox {
	
	private final  Button button;
	private final Label label;

	/**
	 * Initializes a composite, the checkbox and label and adds a grid layout with two columns.
	 * The newly created composite is added to the parent composite passed as a parameter
	 * 
	 * @param labeltext			The text to be displayed as the label
	 * @param selected          boolean, true if the checkbox should initially be selected
	 * @param parent            the parent Composite that the label with checkbox is added to
	 */
	public LabelWithCheckbox(String labeltext, boolean selected, Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(2, false);
		composite.setLayout(layout);
		
		button = new Button(composite, SWT.CHECK);
		button.setSelection(selected);
	
		label = new Label(composite, SWT.NONE);
		label.setText(labeltext);
		composite.setSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}
	
	

	/**
	 * determine whether checkbox is selected
	 * @return			boolean, state of the checkbox
	 */
	public boolean isSelected() {
		return button.getSelection();
	}
	
	/**
	 * get the text displayed
	 * @return String, the label text
	 */
	public String getText() {
		return label.getText();
	}

}
