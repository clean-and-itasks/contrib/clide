package dcl.generator;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * IHandler that handles creation of .dcl files. This handler is called
 * whenever the dcl generation option is selected in the popup menu on .icl files.
 * The Handler gets the path to the icl file and calls the DclGenerator with this information.
 * Finally, the eclipse project view is refreshed so that the new .dcl file is shown.
 * 
 * @author CLIDE 2018-2019
 *
 */
public class DclGenerationHandler implements IHandler {

	
	/**
	 * retrieves the currently selected element in the Eclipse Workbench (the .icl file
	 * selected for .dcl creation) and calls the DclGenerator with its file path. Refreshes eclipse
	 * project of that file once .dcl generation is finished
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// get path of icl file
			IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
			IResource element = (IResource)selection.getFirstElement();
			File path = element.getLocation().toFile();
			
			// create dcl file
			DclGenerator.generateDcl(path);
			
			// refresh project overview window
			IProject project = element.getProject();
			project.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isHandled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void removeHandlerListener(IHandlerListener handlerListener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addHandlerListener(IHandlerListener handlerListener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
