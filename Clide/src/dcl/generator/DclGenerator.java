package dcl.generator;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.text.source.MatchingCharacterPainter;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/** 
 * Utility to generate .dcl file from an .icl file, including imports and
 * definitions specified by the user using a Wizard.
 * 
 * @author CLIDE 2018-2019
 *
 */
public class DclGenerator {
	
	/**
	 * expects the path to an .icl fileas an input. Reads all import and definition statements
	 * from the .icl file and opens a DclWizard dialog with this information
	 */
	public static void generateDcl(File filepath) throws Exception {
		
		// get pach to directory of .icl file
		File directory = filepath.getParentFile();
		
		// open icl file
		Scanner scan = new Scanner(filepath);
		
		
		ArrayList<String> imports = new ArrayList<String>();
		ArrayList<String> definitions = new ArrayList<String>();
		
		// get Clean Module name from filename
		String modulename = filepath.getName();
		modulename = modulename.substring(0, modulename.length()-4);
		
		//check file for imports, function and instance definitions
		while(scan.hasNextLine()) {
			String line = scan.nextLine();
			
			//check whether line is an import
			if (line.startsWith("import") | line.startsWith("from"))
				imports.add(line);
			
			//check whether line is a data type, function or instance definition header
			else if(line.startsWith("instance") | Pattern.matches(".*:: *.*", line))
				definitions.add(line);
		}
	
		scan.close();
		
		// open .dcl generation wizard, pass imports, definitions and module header to wizard object
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		WizardDialog dialog = new WizardDialog(shell, new DclWizard(directory, modulename, imports, definitions));
		dialog.open();
	}
	
	
	/**
	 * creates a dcl file for the specified module (.icl file)
	 * 
	 * @param directory			directory in which to create the .dcl file
	 * @param modulename		module name to be used in .dcl file, also used as filename for new .dcl file
	 * @param imports			list of import statements to be included in .dcl file
	 * @param definitions		list of definition statements to be included in .dcl file
	 */
	public static void createFile(File directory, String modulename, ArrayList<String> imports, ArrayList<String> definitions) throws IOException {
		
		// create new file on disk
		File dclFile = new File(directory.getAbsolutePath() + "/" + modulename + ".dcl");
		dclFile.createNewFile();
		
		// build string that contains content to be written to new file
		String content = "definition module " + modulename + "\n\n";
		
		// add imports
		if(imports.size()>0) {
		String importString = "// Imports\n";
		for(String i : imports)
			importString += i + "\n";
		content += importString;
		}
		
		// add definitions
		if(definitions.size()>0) {
		String definitionString = "// Definitions \n";
		if(imports.size()>0)
			definitionString = "\n" + definitionString;
		for(String d : definitions)
			definitionString += d + "\n\n";
		content += definitionString;
		}
		
		// write content to file
		BufferedWriter writer = new BufferedWriter(new FileWriter(dclFile));
			//writer.write("// This definition module was generated automatically and may contain unused imports and definitions you may not want to export.\n\n");
			writer.write(content);
		writer.close();
	}
	
}