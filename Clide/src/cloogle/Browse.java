package cloogle;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/

/**
* @author CLIDE 2018-2019
*
* Takes care of the browsing part of the cloogle view. 
*/
public class Browse {
	private String url = "https://cloogle.org/";
	private boolean isWindows;
	private boolean isMac;
	private boolean isLinux;
	private Runtime rt;
	private String searchTerm;

	/**
	* Sets the value of the 'searchTerm' field to the value of the parameter
	* which can then be used to launch the appropriate URL.
	*/
	public Browse(String searchTerm) {
		this.searchTerm = searchTerm;
		rt = Runtime.getRuntime();
		generateURL();
		determineOS();
		launchCloogle();
	}

	
	/**
	* The value of 'searchTerm' is appended to the field url, with a # in the
	* middle, resulting in the format used by cloogle when searching for that
	* term.
	*/
	private void generateURL() {
		this.url = this.url+"#"+this.searchTerm;
	}



	/**
	 * Source: https://www.mkyong.com/java/open-browser-in-java-windows-or-linux/
	 * 
	 * Depending on the Operating System, the appropriate command is called such that the
	 * standard browser is launched on the cloogle webpage. 
	 * Straightforward for Windows and IOS, which opens the system's standard browser.
	 * For Linux or Unix an array of browsers is needed, decreasing the robustness, however
	 * most browsers available on these OS's have been specified.
	 */
	private void launchCloogle() {
		try {
			if (isWindows) {
				rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
			} else if (isMac) {
				rt.exec("open " + url);
			} else if (isLinux) {
				
				String[] browsers = {"google-chrome", "chrome", "chromium", "firefox", "opera", "vivaldi", "slimjet", 
						"midori", "mozilla", "conkeror", "konqueror","epiphany", "iridium", "iridium-browser", 
						"falkon", "qupzilla", "epiphany-browser", "kazehakase", "x-www-browser"};
		        
				StringBuffer cmd = new StringBuffer();
		        for (int i=0; i<browsers.length; i++)
		            cmd.append( (i==0  ? "" : " || " ) + browsers[i] +" \"" + url + "\" ");
		        rt.exec(new String[] { "sh", "-c", cmd.toString() });
			} else {
				System.out.println("Operating System not supported by CLIDE");
                return;
           }
		} catch (Exception e) {
			System.out.println("Something went wrong launching https://cloogle.org.");
		}
	}

	/**
	 * Checks the Operating System, and sets the appropriate boolean to true.
	 */
	private void determineOS() {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.indexOf("win") >= 0) {
			isWindows = true;
		} else if (os.indexOf("mac") >= 0) {
			isMac = true;
		} else if (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0) {
			isLinux = true;
		}
	}

}
