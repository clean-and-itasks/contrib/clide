package cloogle;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

/**
* @author CLIDE 2018-2019
*
* Specifies the components of the cloogleview which can be turned on and off in
* the perspective.
*/

public class CloogleView extends ViewPart {
	private Label label;
	private Text text;
	private Button btn;

	public CloogleView() {
		super();

	}

	/**
	 * @see IViewPart.init(IViewSite)
	 */
	public void init(IViewSite site) throws PartInitException {
		super.init(site);
		site.getPart();
	}

	/**
	* Creates the textField together with the different icons, and the starting 
	* text in the textField as part of a composite.
	* Specifies the actions of the return key and the search button as either
	* launching an instance of browse and thereby opening the browser, or if 
	* the search term contains a space the text is replaced by a warning 
	* message.
	*/
	@Override
	public void createPartControl(Composite parent) {

		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(2, true));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		Composite searchComposite = new Composite(container, SWT.NONE);
		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		searchComposite.setLayout(layout);
		searchComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));


		text = new Text(searchComposite, SWT.BORDER | SWT.SEARCH | SWT.ICON_SEARCH | SWT.ICON_CANCEL);
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		text.setText("Search Term (no spaces)");
		text.addKeyListener(new KeyAdapter()
		{	
			public void keyPressed(KeyEvent e)
			{
				if(e.keyCode == SWT.CR || e.keyCode == SWT.LF) {
					if (text.getText().contains(" ")) {
						text.setText("Search term cannot contain spaces!");
					} else {
						String term = text.getText();
						Browse browser = new Browse(term);
					}
				}
			}
		});
		
		
		btn = new Button(searchComposite, SWT.PUSH);
		btn.setText("Search");
		btn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		btn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (text.getText().contains(" ")) {
					text.setText("Search term cannot contain spaces!");
				} else {
					String term = text.getText();
					Browse browser = new Browse(term);
				}
			}
		});		
	}

	@Override
	public void setFocus() {
		label.setFocus();
	}

}

 
