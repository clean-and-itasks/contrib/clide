package handlers;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osgi.service.prefs.BackingStoreException;

import console.ColorConstants;
import console.ConsoleInterfacer;
import preference.ClidePreferenceConstants;

/**
 * AbstractHandler that handles the setting of the CPM.
 * CPM can be set in CLIDE Settings in the menu bar.
 * Handler is called whenever someone selected a file and clicks "Open" in the filemanager.
 * The location to the CPM is set via IEclipsePreferences with the scope set to Instance. 
 * Doing it this way, the CPM location has to be set only once and can be accessed everywhere
 * in the CLIDE code.
 * 
 * @author CLIDE 2018-2019
 *
 */
public class SetCPMHandler extends AbstractHandler {

	private String CPMLocation;
	
	public String getCPMLocation () {
		return CPMLocation;
	}
	
	/**
	 * Called whenever someone opens a file in the filemanager which was
	 * opened by CLIDE Settings --> Set CPM. Location is saved in 
	 * IEclipsePreferences. The key is a constant that is defined in 
	 * ClidePreferenceConstants.
	 * Be aware there is no checking whether cpm.exe is actually
	 * opened in the filemanager.
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(ClidePreferenceConstants.PREFERENCE_ID);
		
		FileDialog fd = new FileDialog(window.getShell());
		fd.setText("Set path to Clean Project Manager (cpm)");
		CPMLocation = fd.open();
		preferences.put(ClidePreferenceConstants.CPM_LOCATION, CPMLocation);
		
		try {
			preferences.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
			ConsoleInterfacer.writeToConsole("Clean-std",
					ColorConstants.sRed("Didn't save settings."));
		}
		
        return null;
	}
	

}
