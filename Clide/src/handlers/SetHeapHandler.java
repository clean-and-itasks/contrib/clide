package handlers;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osgi.service.prefs.BackingStoreException;

import preference.ClidePreferenceConstants;

/**
 * AbstractHandler that handles the setting of the Heap size.
 * Heap size can be set in CLIDE Settings in the menu bar.
 * Whenever user clicks Set Heap Size, this handler is called.
 * 
 * @author CLIDE 2018-2019
 *
 */
public class SetHeapHandler extends AbstractHandler {

	/**
	 * Handler makes the popup window, with default Heap size of 2000M.
	 * A listener is added to the Ok button which will save the Heap size
	 * in the IEclipsePreferences with the scope set to Instance.
	 * A listener is added to the Cancel button which will close the popup
	 * window.
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(ClidePreferenceConstants.PREFERENCE_ID);

		// Making the popup window
		Shell shell = new Shell(window.getShell());
		shell.setText("Set Heap Size");
		shell.setLayout(new GridLayout(2, true));

		Label label = new Label(shell, SWT.NULL);
		label.setText("Please enter a valid heap size:");

		Text text = new Text(shell, SWT.SINGLE | SWT.BORDER);
		text.setText(preferences.get(ClidePreferenceConstants.HEAP_SIZE, "2000M"));

		Button buttonOK = new Button(shell, SWT.PUSH);
		buttonOK.setText("Ok");
		buttonOK.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
		Button buttonCancel = new Button(shell, SWT.PUSH);
		buttonCancel.setText("Cancel");

		// Ok button listener
		buttonOK.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				preferences.put(ClidePreferenceConstants.HEAP_SIZE, text.getText());
				try {
					preferences.flush();
				} catch (BackingStoreException e) {
					e.printStackTrace();
				}
				shell.dispose();
			}
		});

		// Cancel button listener
		buttonCancel.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				shell.dispose();
			}
		});

		shell.pack();
		shell.open();

		return null;
	}

}
