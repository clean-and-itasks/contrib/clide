package perspective;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.IViewLayout;

/**
 * 
 * @author Clide 18-19
 * This class determines which views and action sets are added to the CLIDE perspective
 */
public class ClidePerspectiveFactory implements IPerspectiveFactory {


	@Override
	/**
	 * this function is called when the perspective is opened.
	 * It calls defineActions and defineLayout, which specify which
	 * windows and actions are made available when the perspective is opened.
	 */
	public void createInitialLayout(IPageLayout layout) {
		defineActions(layout);
	    defineLayout(layout);
	}

	/**
	 * This function defines which windows are opened when the CLIDE perspective
	 * is opened as well as their layout.
	 * The CLIDE perspective includes a Project Explorer on the left, Editor Area in the center
	 * and a console at the bottom of the screen.
	 * @param layout
	 */
	private void defineLayout(IPageLayout layout) {
		 // Editors are placed for free.
        String editorArea = layout.getEditorArea();
        
        // Create a column on the left relative to the editor area
        
        IFolderLayout leftColumn =
        		layout.createFolder("leftColumn", IPageLayout.LEFT, 0.25f, editorArea);
        
        leftColumn.addView(IPageLayout.ID_PROJECT_EXPLORER); // Do we want project or package explorer?
        
        IFolderLayout bottom = 
        		layout.createFolder("bottom", IPageLayout.BOTTOM, 0.75f, editorArea); 
        
        bottom.addView("Clide.cloogleview");
        bottom.addView("org.eclipse.ui.console.ConsoleView");
	}
	
	/**
	 * This function defines which action sets are made available when the CLIDE perspective
	 * is opened. The 'launchActionSet' is added to allow the user to run Clean applications
	 * @param layout
	 */
	private void defineActions(IPageLayout layout) {
		// This is where we can add 'actions' to the menu bar
		layout.addActionSet("org.eclipse.debug.ui.launchActionSet");
	}

}
