package console;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.IOConsole;
import org.eclipse.ui.console.IOConsoleOutputStream;

import console.ColorConstants.ConsoleColor;

public class ConsoleInterfacer {
	/**
	 * The Console Interfacer supplies the project with the functionality to write to the console
	 */
	
	public static final String STD_CONSOLE = "Clide-Std";
	

	//private static BlockingQueue<String> q = new LinkedBlockingQueue();
	//public static InputStream inStream;
	/**
	 * used to write to the console, must be called asynchronously, to prevent UI thread error due to unallowed thread access
	 * @param consoleName the Name of the console Page, same names write to same consoles, new name creates new console
	 * @param Message message to be displayed
	 */
	public static void writeToConsoleAsynchronously(String consoleName, String Message){
		//Console writing
		IOConsole clideConsole = findConsole(consoleName);
		IOConsoleOutputStream out = clideConsole.newOutputStream(); // open an output Stream to the Console

		/*
		 * we need to iterate over every char in order to determine its color.
		 */
		for (char c : Message.toCharArray()) {
			//if it is not a color escape char write it to the console - else use it to determine furutre color. 
			if( getColor(c) != null) {
				try {
					//end old stream
					out.close();
					//open new stream
					out = clideConsole.newOutputStream();
					out.setColor(getColor(c));
				} catch (Exception e) {
					System.err.print("issue pipeing in console O/P when changing color");
					e.printStackTrace();
				}
			}else {
				try {
					out.write(c);// write content to console
					out.flush();
				} catch (IOException e) {
					System.err.print("issue pipeing in console O/P");
					e.printStackTrace();
				}
			}
		}
		//add new line to output
		try {
			out.write("\n");
		} catch (IOException e1) {
			System.err.print("issue pipeing in console O/P - end");
			e1.printStackTrace();
		}
		//activates most recent console
		displayConsole(clideConsole);
		
	}
	
	/**
	 * generalized method to ensure it is written to the same console standardized
	 * @param Message message to be written
	 */
	public static void writeToConsole(String Message){
		writeToConsole("Clean-Console", Message);
	}
	
	
	/**
	 * writes Message asynchronously. ensure safety of UI threads
	 * @param consoleName name of the console to write to
	 * @param Message message to be dsiplayed
	 */
	public static void writeToConsole(String consoleName, String Message){
		/**
		 * This runs asynchronously. Could also be synchronously but I guess this is more efficient and hence preferred.
		 */
		Display.getDefault().asyncExec(new Runnable() {
		    public void run() {
		    	writeToConsoleAsynchronously(consoleName, Message);
		    }
		});
	}
	
	/**
	 * activates console given in view i.e. puts it on top
	 * @param clideConsole
	 */
	private static void displayConsole(IOConsole clideConsole) {
		
		// display the console in UI
		//get context data i.e. Workbench->ActiveWindow->ActivePage
		IWorkbench wb = PlatformUI.getWorkbench();
		IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
		if(win==null) 
			System.err.println("Error: no window?!?");
		
		IWorkbenchPage page = win.getActivePage();
		String id = IConsoleConstants.ID_CONSOLE_VIEW; //constant for Console view
		IConsoleView view;
		try {
			view = (IConsoleView) page.showView(id);//get the actual Console View
			view.display(clideConsole);//set display to the just used console
		} catch (PartInitException e) {
			System.out.println("Error occured whilst activating the Console View");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * reads one line form the console (std input console)
	 * @return String array containing the line
	 */
	public static String[] readFromConsole() {
		return readFromConsole("Clean-Input-Console", 1);
	}
	
	/**
	 * reads a set amount of lines form console (std input console)
	 * @param lines amount of lines to be reads
	 * @return String array containing the lines
	 */
	public static String[] readFromConsole(int lines) {
		return readFromConsole("Clean-Input-Console", lines);
	}
	
	
	/**
	 * Read from a certain console a set amount of lines
	 * @param consoleName name of console to be read from
	 * @param lines amount of lines to be read
	 * @return String array containing read lines
	 */
	public static String[] readFromConsole(String consoleName, int lines) {
		
		Display.getDefault().syncExec(new Runnable() {
		    public void run() {
		    	ConsolePlugin plugin = ConsolePlugin.getDefault(); //gets the std console
				IConsoleManager conMan = plugin.getConsoleManager(); 
				
				//Display Console
				IOConsole clideConsole = findConsole(consoleName);
				conMan.addConsoles(new IConsole[] { clideConsole });
				displayConsole(clideConsole);
		    }
		});
		
		/**
		 * The code above runs synchronously. This guarantees that inStream is indeed set afterwards.
		 * What does it do? It finds the specified console, creates it if necessary and displays it.
		 */
		
		IOConsole clideConsole = findConsole(consoleName); 
		// console is guaranteed to be found due to the previous code
		InputStream inStream = clideConsole.getInputStream();
		BufferedReader bf = new BufferedReader(new InputStreamReader(inStream));
		
		try {
			String[] input = new String[lines];
			for(int i = 0; i < lines; i++)
				input[i] = bf.readLine();
			return input;
		} catch (IOException e) {
			return new String[] {};
		}
	}
	

	
	/**
	 * Finds the console view of ours in the currently available views
	 * @param name the name of our console, can be used to have multiple views/consoles for multiple programs
	 * @return the console if found, a new instance otherwise
	 */
	private static IOConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault(); //gets the std console
		IConsoleManager conMan = plugin.getConsoleManager(); 
		IConsole[] existingConsoles = conMan.getConsoles(); //get all 'alive' consoles
		//go through all and try to find mine
		
		for (int i = 0; i < existingConsoles.length; i++)
			if (name.equals(existingConsoles[i].getName()))
				return (IOConsole) existingConsoles[i];
		// no console found, so now we create a new one
		IOConsole clideConsole = new IOConsole(name, null);
		conMan.addConsoles(new IConsole[] { clideConsole }); //add console to available ones
		return clideConsole;
	}

	/**
	 * Returns the color that the char corresponds to, if it is a known escape character
	 * @param t character to be trialed
	 * @return null if unknown, Color otherwise
	 */
	private static Color getColor(Character t) {
		Device device = Display.getCurrent ();
		for(ConsoleColor cc : ConsoleColor.values()) {
			if (cc.character == t) {
				return cc.color;
			}
		}
		return null;
	}

}
