package console.pattern.matcher;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.File;
import java.io.FilenameFilter;
import java.net.URI;
import java.net.URISyntaxException;

import javax.sound.midi.Soundbank;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.debug.ui.console.FileLink;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.ui.console.IPatternMatchListenerDelegate;
import org.eclipse.ui.console.PatternMatchEvent;
import org.eclipse.ui.console.TextConsole;

import project.file.finder.PriorityProjectFileFinder;

/**
 * 
 * @author Clide18/19
 *
 */
public class RootFileMatchListenerDelegate implements IPatternMatchListenerDelegate {

	/**
	 * used if preprocessing is needed on the textconsole
	 */
	@Override
	public void connect(TextConsole console) {

	}

	/**
	 * used if postprocessing is needed on the textconsole
	 */
	@Override
	public void disconnect() {

	}


	@Override
	/**
	 * used if a match to the regex is found in one of the consoles
	 * @param event, the event triggering the link check
	 */
	public void matchFound(PatternMatchEvent event) {
		// first get the actual content
		TextConsole targetTxConsole = (TextConsole) event.getSource();
		String eventContents = targetTxConsole.getDocument().get().substring(event.getOffset(),
				event.getOffset() + event.getLength());
		String file = eventContents;
		int lineNum = -1;
		int cursorOffset = -1; //not used currently since there appears to be an issue with it internaly, causing trouble linking at all
		// process the input, split on , and ; first is always line second cursor pos
		String[] processedFile = file.split("(,|;)");
		int i = processedFile.length;//used to determine splitting
		//splits the output according to what the error parser gives
		if (file.contains(",")) {
			file = processedFile[0];
			if (i > 0) {
				String[] terms = processedFile[1].split(" ");
				lineNum = Integer.valueOf(terms[terms.length-1]);
			}
			i--;
			if (i > 1)
				cursorOffset = Integer.valueOf(processedFile[2]);
		}
		// get File, assumes root dir and only one/first project!
		PriorityProjectFileFinder priorityProjectFileFinder = new PriorityProjectFileFinder(file);
		File targFile = priorityProjectFileFinder.getFile();
		if (targFile != null) {
			//get root path -> add relative part -> get resource located there -> create link
			String base = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();
			String relative = new File(base).toURI().relativize(targFile.toURI()).getPath();
			IFile r = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(relative));
			FileLink fileLink = new FileLink(r, null, -1, -1, lineNum);//link actually used, no cursor position set
			try {
				// Add hyperlink to console
				targetTxConsole.addHyperlink(fileLink, event.getOffset(), event.getLength());
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}else {
			System.out.println("Not found: "+ eventContents);
		}
	}

}
