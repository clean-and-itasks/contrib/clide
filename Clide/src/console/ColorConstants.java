package console;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.swt.graphics.Color;

public class ColorConstants {
	public static ConsoleColor StandardColor = ConsoleColor.BLACK;
	/**
	 * Class for Color in the Console in CLIDE,
	 * each color contains a character [its escape value] and a Color [its color]
	 * @author the1ThousandLines
	 *
	 */
	public enum ConsoleColor{
		//initialize all colors needed so far
		BLACK('\u05f5', new Color(null,0,0,0)),
		RED('\u05f6',new Color(null,200,0,0)),
		BLUE('\u05f7',new Color(null,0,0,200)),
		GREEN('\u05f8',new Color(null,63, 147, 62)),
		YELLOW('\u05f9',new Color(null,220,220,0)),
		PURPLE('\u05f4', new Color(null,84, 38, 109));

		public final char character;
		public final Color color;
		
		/**
		 * Constructor, private
		 * @param c corresponding escape character
		 * @param col corresponding color
		 */
		ConsoleColor(char c, Color col){
			this.character = c;
			this.color = col;
		}
		/**
		 * when converted to string, simply return character, 
		 * allows for nice use in infix + notation
		 */
		@Override
		public String toString() {
			return String.valueOf(character);
		}
	}

	/**
	 * return string holding message that will be black in console
	 * @param msg message to be colored
	 * @return String including right escape character
	 */
	public static String sPurple(String msg) {
		if (msg == null)
			return null;
		return ConsoleColor.PURPLE + msg + StandardColor;
	}
	
	//these methods have the advantage that they always end on being the standard color, 
	// usually black!
	
	/**
	 * return string holding message that will be black in console
	 * @param msg message to be colored
	 * @return String including right escape character
	 */
	public static String sBlack(String msg) {
		if (msg == null)
			return null;
		return ConsoleColor.BLACK + msg + StandardColor;
	}

	/**
	 * return string holding message that will be blue in console
	 * @param msg message to be colored
	 * @return String including right escape character
	 */
	public static String sBlue(String msg) {
		if (msg == null)
			return null;
		return ConsoleColor.BLUE + msg + StandardColor;
	}


	/**
	 * return string holding message that will be red in console
	 * @param msg message to be colored
	 * @return String including right escape character
	 */
	public static String sRed(String msg) {
		if (msg == null)
			return null;
		return ConsoleColor.RED + msg + StandardColor;
	}
	

	/**
	 * return string holding message that will be green in console
	 * @param msg message to be colored
	 * @return String including right escape character
	 */
	public static String sGreen(String msg) {
		if (msg == null)
			return null;
		return ConsoleColor.GREEN + msg + StandardColor;
	}


	/**
	 * return string holding message that will be yellow in console
	 * @param msg message to be colored
	 * @return String including right escape character
	 */
	public static String sYellow(String msg) {
		if (msg == null)
			return null;
		return ConsoleColor.YELLOW + msg + StandardColor;
	}
}

