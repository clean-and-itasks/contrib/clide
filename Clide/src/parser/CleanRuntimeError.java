package parser;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import console.ColorConstants;

/**
 * @author CLIDE 2018-2019
 *
 * Relevant information in a runtime error message:
 * 
 * The text before the first comma defines the type of error.
 * 
 * The text in between the two following single quotation marks defines the ruleName.
 * 
 * The text in between the second two following single quotation marks defines the moduleName.
 * 
 * The remaining text defines what is wrong with the rule.
 */
public class CleanRuntimeError extends CleanError{

	private final String errorType = ColorConstants.sRed("Runtime Error");
	private String moduleName;
	private String ruleName;
	private final String errorMessage =  ColorConstants.sRed(" does not match");
	
	/**
	* Clean runtime errors are always of the same formatting, which is used
	* by the constructor by calling a patternmatcher and assigning the
	* resulting groups to the appropriate fields ruleName and moduleName.
	* @param message, the message classified as a runtime error. 
	*/
	public CleanRuntimeError(String message) {
		super(message);
		Matcher m = Pattern.compile("\\'([^']+)\\'").matcher(message);
		
		m.find();
		ruleName = ColorConstants.sBlue(m.group(1));
		m.find();
		moduleName = ColorConstants.sGreen(m.group(1));

	}
	
	
	/**
	 * Each of the types of information is displayed in purple.
	 * Binds in a String all relevant information into a readable error message
	 * to be displayed in the console.
	 */
	@Override
	public String toString() {
		String print = ColorConstants.sPurple("ErrorType: ") + errorType + "\n"+ColorConstants.sPurple("RuleName: ") + ruleName + ColorConstants.sPurple(", ModuleName: ") 
		+ moduleName + "\n"+ColorConstants.sPurple("ErrorMessage: ") + errorMessage + ColorConstants.sPurple(".");
		//System.out.println(print);
		return super.toString(print);
	}
	
	public String getRuleName() {
		return this.ruleName;
	}
	
	public String getModuleName() {
		return moduleName;
	}
}




