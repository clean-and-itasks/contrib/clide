package parser;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/

/**
* @author CLIDE 2018-2019
*/
public class CleanMessage extends CleanOutput{

	public CleanMessage(String message) {
		super(message);
	}

}
