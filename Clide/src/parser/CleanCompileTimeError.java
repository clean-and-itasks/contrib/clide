package parser;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import console.ColorConstants;

/**
 * @author CLIDE 2018-2019
 *
 * Relevant information contained in all Clean Compile Time errors for classification:
 * 
 * The text before the first square bracket defines the type of error.
 * 
 * The text between the first square bracket and the first comma defines the file in which you work.
 * 
 * The text between the first comma and the last bracket is either the line number and the function name separated by a comma, or just the line number.
 * 
 * The text between the last square bracket and the end of the message is the error message itself.
**/

public class CleanCompileTimeError extends CleanError{
	private String message;
	
	private String errorType;
	private String filename;
	private String lineNr;
	private String functionName;
	private String errorMessage;
	private String warningMessage;
	
	/**
	* Constructor in which the different methods are called in a sequence such that
	* the relevant information mentioned above is captured in the appropriate 
	* fields.
	* @param message, the message classified as a compile time error. 
	*/
	public CleanCompileTimeError(String message) {
		super(message);
		this.message=message;
		errorClassifier();
		fileClassifier();
		lineAndFunctionClassifier();
		errorMessage();
	}
	
	/**
	 * Takes care of: The text between the last square bracket and the end of the message is the 
	 * error message itself. Checks whether there is a value assigned to the warningMessage field, 
	 * and if so the message is an errorMessage and therefore is displayed in red. If not, the 
	 * message is a warningMessage and is displayed in yellow.
	 */
	private void errorMessage() {
		int limit = 2;
		String[] split = message.split("[:]", limit);		
		if(warningMessage != null && !warningMessage.isEmpty())
			errorMessage = ColorConstants.sYellow(split[1].trim()+ ", " + warningMessage);
		else {
			errorMessage = ColorConstants.sRed(split[1].trim());
		}
	}

	/**
	 * Takes care of:  The text between the first comma and the last bracket is either
	 * the line number and the function name separated by a comma, or just the 
	 * line number. If the piece of text that is left after splitting contains a comma, both
	 * the line number and functionName are known, which are then split and assigned to their fields.
	 * If not, then just the lineNumber is known, and assigned to its field
	 * The remaining message is saved to the field 'message'. The lineNrs are displayed
	 * in black in the console, while the functionNames are blue.
	 */
	private void lineAndFunctionClassifier() {
		int limit = 2;
		String[] split = message.split("\\]", limit);
		String potFurtherSplit = split[0];
		if(potFurtherSplit.contains(",")) {
			String[] secondSplit = potFurtherSplit.split("[,]");
			lineNr = ColorConstants.sBlack(secondSplit[0]);
			functionName = ColorConstants.sBlue(secondSplit[1]);
		} else {
			lineNr = ColorConstants.sBlack(potFurtherSplit);
			functionName = ColorConstants.sBlue("corresponding functionName unknown");
		}
		this.message = split[1];
	}

	/**
	 * Takes care of: The text between the first square bracket and the first comma defines the file 
	 * in which you work.
	 * Splits off the message at the first comma encountered, assigning the part before the comma
	 * to 'fileName', and the part after to 'message', to allow for step-by-step assigning of relevant
	 * information. The fileName is displayed in green in the console.
	 */
	private void fileClassifier() {
		int limit = 2;
		String[] split = message.split("[,]", limit);
		this.filename = ColorConstants.sGreen(split[0]);
		this.message = split[1];
	}
	
	
	/**
	 * Takes care of: The text before the first square bracket defines the type of error.
	 * Splits off the message at the first bracket (including the one whitespace before it), assigning 
	 * the part before the bracket to the field 'errorType'.
	 * Then the remaining part of the message is assigned to 'message' to allow step-by-step assigning 
	 * of relevant information.
	 * In case there is a warning, such as in the overloading error, the warningClassifier is called.
	 * The errorType is displayed in red in the console.
	 */
	private void errorClassifier() {
		int limit = 2;
		String[] split = message.split(" \\[", limit);
		if(!split[0].equals("Warning")) {
			this.errorType = ColorConstants.sRed(split[0]);
			this.message = split[1];
		}
		else {
			warningClassifier();
		}
	}

	/**
	 * In case there is a warning, such as in the overloading error, this warningClassifier is called,
	 * which saves the warning message into a separate field, such that it can later be added to the errormessage.
	 * It also calls errorClassifier on the remaining part of the message which contains the actual error.
	 * The warningMessage is displayed in yellow in the console.
	 */
	private void warningClassifier() {
		int limit = 2;
		String[] split = message.split("\\n", limit);
		warningMessage = ColorConstants.sYellow(split[0]);
		message = split[1];
		errorClassifier();
	}

	public String getFilename() {
		return filename;
	}
	
	public String getLineNr() {
		return lineNr;
	}
	
	public String getFunctionName() {
		return this.functionName;
	}
	
	public String getErrorMessage() {
		return this.errorMessage;
	}
	
	
	/**
	 * Each of the types of information is displayed in purple.
	 * Binds in a String all relevant information into a readable error message
	 * to be displayed in the console.
	 */
	@Override
	public String toString() {
		String print = ColorConstants.sPurple("ErrorType: ") + errorType + "\n"+ ColorConstants.sPurple("ModuleName: ") + filename + 
		ColorConstants.sPurple(", LineNumber: ") + lineNr + ColorConstants.sPurple(", RuleName: ") + functionName
		+"\n"+ ColorConstants.sPurple("Error-message: ") + errorMessage + ColorConstants.sPurple(".");
		return super.toString(print);
	}
	
}




















