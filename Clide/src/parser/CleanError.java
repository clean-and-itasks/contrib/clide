package parser;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import console.ColorConstants;

/**
* @author CLIDE 2018-2019
*/
public class CleanError extends CleanOutput{
	
	public CleanError(String message) {
		super(message);
	}
	
	
	/**
	* Encodes the message in a String with a red colour.
	*/
	@Override
	public String toString(String message) {
		return ColorConstants.sRed(message);
	}
	

}
