package parser;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/

/**
* @author CLIDE 2018-2019
*
* Abstract class that defines the general type of message that will in the end
* be returned, whether it is a compile time, runtime, or normal message.
*/
public abstract class CleanOutput {
	
	private String text;
	
	public CleanOutput(String message) {
		this.text = message;
	}



	public String toString(String message) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
