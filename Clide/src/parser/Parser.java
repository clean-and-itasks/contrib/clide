package parser;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.util.regex.Pattern;

/** 
 * @author CLIDE 2018-2019
 *
 * CompileTime regex catches any expression that contains square brackets,
 * and also catches compile time errors spanning multiple lines due to (.|\n)*
 */
public class Parser {

	/**
	* 'parse' selects the type of error through the different if-statements.
	* First it is determined whether the output is a run time error message.
	* If not, there are a few types of compile time error message, each
	* with their own formatting, which need to be adapted to be passed
	* to the CompilTimeError class. If the message does not fit one 
	* of the regular expressions for an error message, it is passed in the
	* end to the CleanMessage class to be parsed as is and fed back to the
	* console.
	*/
	public CleanOutput parse(String output) {
		if(output.startsWith("Run time error")) {
			CleanRuntimeError er = new CleanRuntimeError(output);
			er.toString();
			return er; 
		}

		else if(Pattern.matches(".*\\[.*\\](.|\n)*", output)&&output.startsWith("Bring up to date...Compiling...Analyzing ")) {
			String[] split = output.split("Bring up to date...Compiling...Analyzing ", 2);
			String bringUpto = split[0];
			String rest = split[1];
			
			String[] split2 = rest.split("\\.", 2); 

			String fileName = split2[0];
			String rest2 = split2[1];
			String[] split3 = rest2.split(fileName+"\\.", 2);

			String newOutput = split3[1]+ bringUpto; //+ split3[0]; -> (no abc fileCompiling 'test')
			CleanCompileTimeError er =  new CleanCompileTimeError(newOutput);
			er.toString();
			return er;
		}
		else if(Pattern.matches(".*\\[.*\\](.|\n)*", output)) {
			CleanCompileTimeError er =  new CleanCompileTimeError(output);
			er.toString();
			return er;
		}
		else {
			return new CleanMessage(output);
		}
	}
}

