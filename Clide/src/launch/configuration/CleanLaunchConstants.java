package launch.configuration;

/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/

/**
 * Interface for keeping the Launch Constants.
 * Should eventually be replaced by final class with static attributes as this is bad style.
 * 
 * @author CLIDE 2018-2019
 */

public interface CleanLaunchConstants {
	
    
    public String PROJECT_NAME = "Clean Project";

    public String CPM_LOCATION = "Clean Project Manager location";
    
    public String ENVIRONMENT_LOCATION = "Clean Environment Location";
    
    public String LAUNCH_CONFIG_TYPE_ID = "Clide.launchConfigurationTypes.CleanLauncher";

    public String DEPENDENCY_DIR = "Path variable for external environments";
    
    public String HEAP_SIZE = "Heap Size";
    
    public String STACK_SIZE = "StackSize";


}