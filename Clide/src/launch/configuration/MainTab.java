package launch.configuration;

/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import preference.ClidePreferenceConstants;

/**
 * Class responsible for the Main Tab of the Launch Configuration Dialog.
 * Concerns itself with creating the GUI and saving the inputs in the launch configuration.
 * 
 * @author CLIDE 2018-2019
 */

public class MainTab extends AbstractLaunchConfigurationTab implements ILaunchConfigurationTab {
	

	private boolean fDisableUpdate = false;

	private IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(ClidePreferenceConstants.PREFERENCE_ID);
	private Text fProjectName;
	private Text fCpmLocation;
	private Text fExternalPath;
	private Text fEnvironmentPath;

	/**
	 * sets sensible defaults for the launch configuration
	 * @see AbstractLaunchConfigurationTab#setDefaults(ILaunchConfigurationWorkingCopy)
	 */
	@Override
	public void setDefaults(final ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(CleanLaunchConstants.CPM_LOCATION,
				preferences.get(ClidePreferenceConstants.CPM_LOCATION, ""));
		configuration.setAttribute(CleanLaunchConstants.PROJECT_NAME, "");
		configuration.setAttribute(CleanLaunchConstants.ENVIRONMENT_LOCATION, "");
		configuration.setAttribute(CleanLaunchConstants.DEPENDENCY_DIR, "");
	}

	/**
	 * initializes the input fields with values from the launch configuration if existent.
	 * @see AbstractLaunchConfigurationTab#initializeFrom(ILaunchConfiguration)
	 */
	@Override
	public void initializeFrom(final ILaunchConfiguration configuration) {
		fDisableUpdate = true;

		try {
			fCpmLocation.setText(configuration.getAttribute(CleanLaunchConstants.CPM_LOCATION,
					preferences.get(ClidePreferenceConstants.CPM_LOCATION, "")));
			fProjectName.setText(configuration.getAttribute(CleanLaunchConstants.PROJECT_NAME, ""));
			fEnvironmentPath.setText(configuration.getAttribute(CleanLaunchConstants.ENVIRONMENT_LOCATION, ""));
			fExternalPath.setText(configuration.getAttribute(CleanLaunchConstants.DEPENDENCY_DIR, ""));
		} catch (CoreException e) {
			System.out.println("Exception occurred in setting up run configuration main tab");
		}

		fDisableUpdate = false;
	}

	/**
	 * saves the text of the input fields in the launch configuration
	 * @see AbstractLaunchConfigurationTab#performApply(ILaunchConfigurationWorkingCopy)
	 */
	@Override
	public void performApply(final ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(CleanLaunchConstants.CPM_LOCATION, fCpmLocation.getText());
		configuration.setAttribute(CleanLaunchConstants.PROJECT_NAME, fProjectName.getText());
		configuration.setAttribute(CleanLaunchConstants.ENVIRONMENT_LOCATION, fEnvironmentPath.getText());
		configuration.setAttribute(CleanLaunchConstants.DEPENDENCY_DIR, fExternalPath.getText());
	}

	
	/**
	 * checks if user inputs are valid.
	 * currently always return true, i.e. all user input is accepted
	 * @see ILaunchConfigurationTab#isValid(ILaunchConfiguration)
	 */
	@Override
	public boolean isValid(final ILaunchConfiguration launchConfig) {
		// Ideas: check if compiler location is correct

		return true;
	}

	/**
	 * @see ILaunchConfigurationTab#canSave()
	 */
	@Override
	public boolean canSave() {
		// allow save when a file location is entered - no matter if the file
		// exists or not
		return true;// (!fTxtProject.getText().isEmpty()) && (!fTxtFile.getText().isEmpty());
	}

	/**
	 * @see ILaunchConfigurationTab#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Main Tab";
	}

	/**
	 * @see ILaunchConfigurationTab#getMessage()
	 */
	@Override
	public String getMessage() {
		return "Global";
	}

	/**
	 * creates the GUI of the Tab
	 * @see ILaunchConfigurationTab#createControl(Composite)
	 */
	@Override
	public void createControl(final Composite parent) {
		Composite topControl = new Composite(parent, SWT.NONE);
		topControl.setLayout(new GridLayout(1, false));

		// browse clean
		Group fFirstGroup = new Group(topControl, SWT.NONE);
		fFirstGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		fFirstGroup.setText("Clean Project Manager (cpm) location");
		fFirstGroup.setLayout(new GridLayout(2, false));

		fCpmLocation = new Text(fFirstGroup, SWT.BORDER);

		fCpmLocation.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if (!fDisableUpdate)
					updateLaunchConfigurationDialog();
			}
		});

		fCpmLocation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Button fBtnBrowseClean = new Button(fFirstGroup, SWT.NONE);
		fBtnBrowseClean.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				FileDialog ch = new FileDialog(getShell());

				ch.setText("Select clean compiler");
				String fPath = ch.open();
				fCpmLocation.setText(fPath);

			}
		});
		fBtnBrowseClean.setText("Browse...");

		// browse file
		Group fSecondGroup = new Group(topControl, SWT.NONE);
		fSecondGroup.setLayout(new GridLayout(2, false));
		fSecondGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		fSecondGroup.setText("Project");

		fProjectName = new Text(fSecondGroup, SWT.BORDER);

		fProjectName.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if (!fDisableUpdate)
					updateLaunchConfigurationDialog();
			}
		});

		fProjectName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Button fBtnBrowseFile = new Button(fSecondGroup, SWT.NONE);
		fBtnBrowseFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(parent.getShell(),
						new WorkbenchLabelProvider(), new ProjectContentProvider());
				dialog.setTitle("Select project");
				dialog.setMessage("Select the project hosting your text file:");
				dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
				if (dialog.open() == Window.OK)
					fProjectName.setText(((IResource) dialog.getFirstResult()).getName());
			}
		});
		fBtnBrowseFile.setText("Browse...");

		// browse env
		Group fThirdGroup = new Group(topControl, SWT.NONE);
		fThirdGroup.setLayout(new GridLayout(2, false));
		fThirdGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		fThirdGroup.setText("Environment file location (.env)");

		fEnvironmentPath = new Text(fThirdGroup, SWT.BORDER);

		fEnvironmentPath.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if (!fDisableUpdate)
					updateLaunchConfigurationDialog();
			}
		});

		fEnvironmentPath.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Button fBtnBrowseEnv = new Button(fThirdGroup, SWT.NONE);
		fBtnBrowseEnv.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				FileDialog ch = new FileDialog(getShell());

				ch.setText("Select environment file");
				String fPath = ch.open();
				// System.out.println(fPath);
				fEnvironmentPath.setText(fPath);

			}
		});
		fBtnBrowseEnv.setText("Browse...");

		// Option to add directory with module dependencies outside root project folder
		Group fDependencyGroup = new Group(topControl, SWT.NONE);
		fDependencyGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		fDependencyGroup.setText("Add (multiple) directories with project dependencies");
		fDependencyGroup.setLayout(new GridLayout(3, false));

		fExternalPath = new Text(fDependencyGroup, SWT.BORDER);
		fExternalPath.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		fExternalPath.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if (!fDisableUpdate)
					updateLaunchConfigurationDialog();
			}
		});

		fExternalPath.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Button fBtnBrowseExternal = new Button(fDependencyGroup, SWT.NONE);
		fBtnBrowseExternal.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				DirectoryDialog chooseDir = new DirectoryDialog(getShell());
				String separator = ";";
				if (fExternalPath.getText().equals("")) {
					fExternalPath.append(chooseDir.open());
				} else {
					// Introduce a separator that allows adding multiple paths sequentially in one
					// string
					fExternalPath.append(separator + chooseDir.open());
				}
			}
		});
		fBtnBrowseExternal.setText("Browse...");

		setControl(topControl);
	}

}
