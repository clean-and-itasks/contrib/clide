package launch.configuration;

/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;

/**
 * Class responsible for adding our Tabs to the Launch Configuration Dialog.
 * Gets called by plugin.
 * 
 * @author CLIDE 2018-2019
 */

public class LaunchConfigurationTabGroupClean extends AbstractLaunchConfigurationTabGroup {



	@Override
	public void createTabs(ILaunchConfigurationDialog dialog, String mode) {
		setTabs(new ILaunchConfigurationTab[] { new MainTab(), new SettingsTab(),new CommonTab() });

	}

}
