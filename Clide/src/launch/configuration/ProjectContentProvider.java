package launch.configuration;

/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/

import org.eclipse.jface.viewers.ITreeContentProvider;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;

/**
 * This class provides the project selection dialog of the main tab with content, e.g. folder, files, etc.
 * In this case, we only provide projects.
 * 
 * @author CLIDE 2018-2019
 */

public class ProjectContentProvider extends BaseWorkbenchContentProvider implements ITreeContentProvider {
	


	/**
	 * checks if a given object is a project and returns it if true
	 * @see ITreeContentProvider#getChildren(Object)
	 */
    @Override
    public Object[] getChildren(final Object element) {
        if (element instanceof IProject)
            return new Object[0];

        return super.getChildren(element);
    }
}
