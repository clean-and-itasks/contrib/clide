package launch.configuration;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import preference.ClidePreferenceConstants;

/**
 * similar to {@link MainTab}
 * Only difference is that it allows to set the heap and stack size.
 * 
 * @author CLIDE 2018-2019
 */

public class SettingsTab extends AbstractLaunchConfigurationTab implements ILaunchConfigurationTab {
	

	private boolean fDisableUpdate = false;

	private IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(ClidePreferenceConstants.PREFERENCE_ID);
	private Text fStackSize;
	private Text fHeapSize;
	
	/**
	 * @see AbstractLaunchConfigurationTab#createControl(Composite)
	 */
	@Override
	public void createControl(Composite parent) {

		Composite topControl = new Composite(parent, SWT.NONE);
		topControl.setLayout(new GridLayout(1, false));

		// browse clean
		Group fFirstGroup = new Group(topControl, SWT.NONE);
		fFirstGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		fFirstGroup.setText("Heap and Stack size");
		fFirstGroup.setLayout(new GridLayout(2, false));

		fHeapSize = new Text(fFirstGroup, SWT.BORDER);
		fStackSize = new Text(fFirstGroup, SWT.BORDER);

		fHeapSize.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if (!fDisableUpdate) 
						updateLaunchConfigurationDialog();
			}
		});

		fStackSize.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if (!fDisableUpdate) 
						updateLaunchConfigurationDialog();
			}
		});

		fHeapSize.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		fStackSize.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		setControl(topControl);

	}

	/**
	 * @see AbstractLaunchConfigurationTab#setDefaults(ILaunchConfigurationWorkingCopy)
	 */
	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {

		configuration.setAttribute(CleanLaunchConstants.HEAP_SIZE, preferences.get(ClidePreferenceConstants.HEAP_SIZE, "2000M"));
		configuration.setAttribute(CleanLaunchConstants.STACK_SIZE, preferences.get(ClidePreferenceConstants.STACK_SIZE, "200M"));

	}

	/**
	 * @see AbstractLaunchConfigurationTab#initializeFrom(ILaunchConfiguration)
	 */
	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {

		fDisableUpdate = true;

		try {

			fHeapSize.setText(
					configuration.getAttribute(CleanLaunchConstants.HEAP_SIZE, preferences.get(ClidePreferenceConstants.HEAP_SIZE, "2000M")));
			fStackSize.setText(
					configuration.getAttribute(CleanLaunchConstants.STACK_SIZE, preferences.get(ClidePreferenceConstants.STACK_SIZE, "200M")));
		} catch (CoreException e) {
			System.out.println("Exception occurred in setting up run configuration main tab");
		}

		fDisableUpdate = false;

	}

	/**
	 * @see AbstractLaunchConfigurationTab#performApply(ILaunchConfigurationWorkingCopy)
	 */
	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(CleanLaunchConstants.HEAP_SIZE, fHeapSize.getText());
		configuration.setAttribute(CleanLaunchConstants.STACK_SIZE, fStackSize.getText());

	}

	/**
	 * @see AbstractLaunchConfigurationTab#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Settings Tab";
	}

}
