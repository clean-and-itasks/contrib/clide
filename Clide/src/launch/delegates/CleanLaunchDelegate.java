package launch.delegates;
/*
BSD 2-Clause License

Copyright (c) 2019, Edwin Wenink, Anouk Prins, Bram Pol, Nick Stracke, Johannes 
Löwe, Elena Kreis, and Daniel Anthes
All rights reserved.
*/
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.debug.ui.ILaunchShortcut2;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import console.ColorConstants;
import console.ConsoleInterfacer;
import launch.configuration.CleanLaunchConstants;
import main.file.finder.MainFileFinder;
import parser.CleanOutput;
import parser.Parser;
import preference.ClidePreferenceConstants;
import services.ExternalCommandService;

/**
 * ILaunchConfigurationDelegate, ILaunchShortcut and ILaunchShortcut2 class for 
 * delegating the launching of files in CLIDE.
 * 
 * @author CLIDE 2018-2019
 *
 */
public class CleanLaunchDelegate implements ILaunchConfigurationDelegate, ILaunchShortcut, ILaunchShortcut2 {

	private Parser parser;
	private String consoleName;
	private final String DEFAULT_HEAP_SIZE = "2000M";
	private final String DEFAULT_STACK_SIZE = "200M";

	public CleanLaunchDelegate() {
		parser = new Parser();
		consoleName = "CLIDE-std";
	}

	/**
	 * The launch object has already been registered with the launch manager.
	 * CPM location, main .icl file, project location and possible environments and/or
	 * dependencies are saved. Then, a Clean project is created. After, the Heap
	 * and Stack size are set and possible module dependencies and environments.
	 * After all this, the project is compiled and run.
	 * 
	 * @param configuration
	 * @param mode
	 * @param launch
	 * @param monitor
	 * @throws CoreException
	 */
	@Override
	public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
			throws CoreException {

		// save editors before launching
		saveAllEditors();

		// Load Clean Project Manager (cpm) location from the run configurations.
		String cpmLocation = configuration.getAttribute(CleanLaunchConstants.CPM_LOCATION, "");

		// Do not proceed and print an error to console if the cpm location is not set
		// yet.
		if (cpmLocation.equals("")) {
			ConsoleInterfacer.writeToConsole(this.consoleName,
					ColorConstants.sRed("Set the Clean Project Manager (cpm) location first."));
			return;
		}

		// Get clean relevant information (from the run configuration)
		IFile mainIclFile = getMainIclFile(configuration, true);
		Path mainIclFilePath = getmainIclFilePath(mainIclFile);
		String cleanProjectLocation = mainIclFilePath.getParent().toString();
		String mainIclName = mainIclFile.getName().replace(".icl", "");
		String cleanProjectName = mainIclName + ".prj";
		// extra stuff
		String environmentPath = configuration.getAttribute(CleanLaunchConstants.ENVIRONMENT_LOCATION, "");
		String dependencyPaths = configuration.getAttribute(CleanLaunchConstants.DEPENDENCY_DIR, "");

		// Create a project (.prj) file for the main .icl file of the project.
		Boolean createProjectSuccess = createCleanProject(cpmLocation, mainIclFilePath.toString());
		if (!createProjectSuccess)
			return;

		// Set heap and stack size for the project as specified in the run
		// configuration, otherwise set default values
		setHeapStackSize(configuration, cpmLocation, cleanProjectName, cleanProjectLocation);

		// Add external module dependencies, either as separate modules or through an
		// .env environment file.
		addModuleDependencies(dependencyPaths, cpmLocation, cleanProjectName, cleanProjectLocation);
		addEnvironment(environmentPath, cpmLocation, cleanProjectName, cleanProjectLocation);

		// Build an executable / binary file (MS Windows/UNIX) from the .prj project
		// file
		compileProject(cpmLocation, cleanProjectName, cleanProjectLocation);

		// Execute the compiled executable / binary
		runProject(cpmLocation, mainIclName, cleanProjectLocation);

	}

	/**
	 * Checks if the operating system is Windows
	 * 
	 * @return true if OS is MS Windows
	 */
	private Boolean isWindowsOS() {
		String os = System.getProperty("os.name").toLowerCase();
		return os.indexOf("win") >= 0;
	}

	private void saveAllEditors() {
		Display.getDefault().syncExec(new Runnable() {
			public void run() {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().saveAllEditors(true);
			}
		});
	}

	/**
	 * This function reads the main .icl file location from the run configuration
	 * 
	 * @param configuration
	 * @return a Path object of the main .icl file path
	 * @throws CoreException
	 */
	private IFile getMainIclFile(ILaunchConfiguration configuration, boolean promptUser) throws CoreException {
		String projectName = configuration.getAttribute(CleanLaunchConstants.PROJECT_NAME, "");

		if (!projectName.isEmpty()) {
			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			IFile[] files = MainFileFinder.getMainFiles(project);
			if (files.length == 1)
				return files[0];
			else if (promptUser) {
				// Asked user to select one main file
				// For now just communicate it via the console
				ConsoleInterfacer.writeToConsole(this.consoleName,
						ColorConstants
								.sRed("Error: There are " + files.length + " files with a Start function in project "
										+ projectName + "! Your project must contain EXACTLY one main file."));
			}
		}

		return null;
	}

	/**
	 * Returns the path for the IFile given.
	 * 
	 * @param file
	 * @return
	 * @throws CoreException
	 */
	private Path getmainIclFilePath(IFile file) throws CoreException {
		if (file != null && file.exists())
			return Paths.get(file.getLocationURI());
		else {
			ConsoleInterfacer.writeToConsole(this.consoleName,
					ColorConstants.sRed("Error while launch: Please try again!"));
			throw new CoreException(Status.CANCEL_STATUS);
		}
	}

	/**
	 * Creates a clean project for the file of mainIclFilePath
	 * 
	 * @param ex              singleton instance of ExternalCommandService
	 * @param cpmLocation
	 * @param mainIclFilePath
	 * @param projectLocation
	 * @param parser
	 * @param consoleName
	 * @return A String array with the command output (index 0) and error trace
	 *         (index 1)
	 */
	private Boolean createCleanProject(String cpmLocation, String mainIclFilePath) {
		ExternalCommandService ex = ExternalCommandService.getInstance();
		List<String> project = Arrays.asList(cpmLocation, "project", mainIclFilePath, "create");
		try {
			String[] createProjectOutput = ex.runExternalCommand(project);

			// If the error trace is not empty, we report the error to console and do not
			// proceed
			// CHECK THIS LINE ON LINUX:
			// for success the cpm (on Windows at least) prints null for regular output, but
			// "" for no errors.
			if (!(createProjectOutput[1].equals(""))) {
				CleanOutput output = this.parser.parse(createProjectOutput[1]);
				ConsoleInterfacer.writeToConsole(consoleName,
						ColorConstants.sRed("\n \nProgram Errors [Compile time]:\n") // Isn't this runtime and not
																						// compile time?
								+ output);
				return false;
			} else {
				ConsoleInterfacer.writeToConsole(this.consoleName, ColorConstants.sGreen("Created project"));
				return true;
			}

		} catch (IOException exception) {
			ConsoleInterfacer.writeToConsole(this.consoleName, ColorConstants
					.sRed("\n \nProgram Errors [Compile time]:\n Failed to create project due to IOException"));
			return false;

		}
	}

	/**
	 * Sets heap and stack size as specified in the run configuration In case no
	 * preferences are set in the run configuration, this method sets sensible
	 * default values Which are 2000M heap size, and 200M stack size.
	 * 
	 * @param configuration
	 * @param ex               singleton instance of ExternalCommandService
	 * @param cpmLocation
	 * @param cleanProjectName
	 * @param projectLocation
	 * @throws CoreException
	 */
	private void setHeapStackSize(ILaunchConfiguration configuration, String cpmLocation, String cleanProjectName,
			String cleanProjectLocation) throws CoreException {
		ExternalCommandService ex = ExternalCommandService.getInstance();
		String heapSize = configuration.getAttribute(CleanLaunchConstants.HEAP_SIZE, this.DEFAULT_HEAP_SIZE);
		String stackSize = configuration.getAttribute(CleanLaunchConstants.STACK_SIZE, this.DEFAULT_STACK_SIZE);
		List<String> adjustSettings = Arrays.asList(cpmLocation, "project", cleanProjectName, "set", "-h", heapSize,
				"-s", stackSize);
		String[] commandOutput = new String[0];
		try {
			commandOutput = ex.runExternalCommand(adjustSettings, cleanProjectLocation);

			if (commandOutput[1].equals("")) {
				ConsoleInterfacer.writeToConsole(this.consoleName,
						ColorConstants.sGreen("Heap- and/or stack size successfully set in project file"));
			} else {
				ConsoleInterfacer.writeToConsole(this.consoleName, ColorConstants
						.sRed("An error occurred while setting heap- and stack size: " + commandOutput[1]));
			}

		} catch (IOException e) {
			ConsoleInterfacer.writeToConsole(this.consoleName,
					ColorConstants.sRed("IO Exception: please provide a correct command"));
			e.printStackTrace();
		}

	}

	/**
	 * Reads the run configuration for separate module dependencies (so not an .env
	 * environment but individual modules) These individual module dependencies are
	 * stored in a String of form "mod1:mod2" Each dependency is added separately
	 * Reports success or error to user
	 * 
	 * @param dependencyPaths
	 */
	private void addModuleDependencies(String dependencyPaths, String cpmLocation, String cleanProjectName,
			String cleanProjectLocation) {

		ExternalCommandService ex = ExternalCommandService.getInstance();

		if (dependencyPaths != "") {
			String[] paths = dependencyPaths.split(";");

			for (String path : paths) {
				List<String> addDependency = Arrays.asList(cpmLocation, "project", cleanProjectName, "path", "add",
						path);
				String[] dependency;
				try {
					dependency = ex.runExternalCommand(addDependency, cleanProjectLocation);
					if (dependency[1].equals("")) {
						ConsoleInterfacer.writeToConsole(this.consoleName,
								ColorConstants.sGreen(dependency[0] + ": added " + path));
					} else {
						ConsoleInterfacer.writeToConsole(this.consoleName, ColorConstants.sRed(dependency[1]));
					}

				} catch (IOException e) {
					ConsoleInterfacer.writeToConsole(this.consoleName,
							ColorConstants.sRed("IO Exception: please provide a correct command"));
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Read the .env file from the run configuration, and adds it as a dependency to
	 * the .prj file Reports success or error to user
	 * 
	 * @param ex
	 * @param environmentPath
	 * @param cpmLocation
	 * @param cleanProjectName
	 * @param projectLocation
	 * @param consoleName
	 */
	private void addEnvironment(String environmentPath, String cpmLocation, String cleanProjectName,
			String cleanProjectLocation) {

		ExternalCommandService ex = ExternalCommandService.getInstance();

		if (!environmentPath.isEmpty()) {
			String[] addEnv;
			try {
				addEnv = ex.runExternalCommand(
						Arrays.asList(cpmLocation, "project", cleanProjectName, "target", environmentPath),
						cleanProjectLocation);
				if (addEnv[1].equals("")) {
					// This command does not seem to have output on success, so I added a manual
					// success statement
					// ConsoleInterfacer.writeToConsole(consoleName,
					// ColorConstants.sBlue(addEnv[1]));
					ConsoleInterfacer.writeToConsole(consoleName,
							ColorConstants.sGreen("Succesfully added environment file to the project"));
				} else {
					ConsoleInterfacer.writeToConsole(this.consoleName, ColorConstants.sRed(addEnv[0]));
				}
			} catch (IOException e) {
				ConsoleInterfacer.writeToConsole(this.consoleName,
						ColorConstants.sRed("IOException: please provide a correct command"));
				e.printStackTrace();
			}
		}
	}

	/**
	 * Builds an executable/binary by compiling the project as specified in the .prj
	 * project file Reports success or error to user
	 * 
	 * @param ex
	 * @param cpmLocation
	 * @param cleanProjectName
	 * @param projectLocation
	 * @param consoleName
	 * @param parser
	 */
	private void compileProject(String cpmLocation, String cleanProjectName, String cleanProjectLocation) {

		ExternalCommandService ex = ExternalCommandService.getInstance();

		String[] results;
		try {
			results = ex.runExternalCommand(Arrays.asList(cpmLocation, "project", cleanProjectName, "build"),
					cleanProjectLocation);
			if (results[0] != null && results[0].toLowerCase().contains("error")) {
				// Why does this code use the "normal output" and ignores the error stream at
				// index 1?
				// Does this produce correct results?
				CleanOutput output = this.parser.parse(results[0]);
				ConsoleInterfacer.writeToConsole(consoleName, ColorConstants.sRed("\n \nProgram Errors:\n") + output);
				return;
			} else {
				ConsoleInterfacer.writeToConsole(this.consoleName,
						ColorConstants.sGreen("Succesfully compiled project!"));
			}

		} catch (IOException e) {
			ConsoleInterfacer.writeToConsole(this.consoleName,
					ColorConstants.sRed("IO Exception: please provide a correct command"));
			e.printStackTrace();
		}
	}

	/**
	 * Checks for the OS to determine whether an executable (Windows) or a binary
	 * (Linux) has to be executed Then executes this executable/binary and shows the
	 * user the output Shows the user the error message on failure
	 * 
	 * @param ex
	 * @param cpmLocation
	 * @param executable
	 * @param projectLocation
	 * @param consoleName
	 * @param parser
	 */
	private void runProject(String cpmLocation, String executable, String cleanProjectLocation) {

		ExternalCommandService ex = ExternalCommandService.getInstance();

		List<String> execute = Arrays.asList("");
		if (isWindowsOS())
			execute = Arrays.asList(cleanProjectLocation + "\\" + executable + ".exe");
		else
			execute = Arrays.asList("./" + executable);

		String[] executed;
		try {
			executed = ex.runExternalCommand(execute, cleanProjectLocation);
			ConsoleInterfacer.writeToConsole(this.consoleName,
					ColorConstants.sBlue("Program returned:\n") + executed[0]);
			/*
			 * if ((executed[1] == null || executed[1].equals("")) &&
			 * !executed[0].contains("error")) {
			 * ConsoleInterfacer.writeToConsole(consoleName,
			 * ColorConstants.sGreen("No Errors!\n")); } else
			 */
			if (executed[1].contains("Run time")) {
				ConsoleInterfacer.writeToConsole(this.consoleName,
						ColorConstants.sRed("\n \nProgram Errors [Run Time]:\n") + this.parser.parse(executed[1]));
				// + ColorConstants.sRed(executed[1]));
			} else {
				return;
			}
		} catch (IOException e) {
			ConsoleInterfacer.writeToConsole(this.consoleName,
					ColorConstants.sRed("IOException: please provide a correct command"));
			e.printStackTrace();
		}

	}

	@Override
	public ILaunchConfiguration[] getLaunchConfigurations(ISelection selection) {
		return getLaunchConfgurations(getLaunchableResource(selection));
	}

	@Override
	public ILaunchConfiguration[] getLaunchConfigurations(IEditorPart editorpart) {
		return getLaunchConfgurations(getLaunchableResource(editorpart));
	}

	@Override
	public IResource getLaunchableResource(ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			for (final Object element : ((IStructuredSelection) selection).toArray()) {
				if (element instanceof IResource)
					return (IResource) element;
			}
		}

		return null;
	}

	@Override
	public IResource getLaunchableResource(IEditorPart editorpart) {
		final IEditorInput input = editorpart.getEditorInput();
		if (input instanceof FileEditorInput)
			return ((FileEditorInput) input).getFile();

		return null;
	}

	@Override
	public void launch(ISelection selection, String mode) {
		launchShortcut(getLaunchableResource(selection), mode);
	}

	@Override
	public void launch(IEditorPart editor, String mode) {
		launchShortcut(getLaunchableResource(editor), mode);
	}

	/**
	 * Gets the launch configurations from the ILaunchManager. From this configuration,
	 * the main .icl file is retrieved and if this is equal to the resource, it is added
	 * to the array of launch configurations that is returned.
	 * 
	 * @param resource
	 * @return launchconfigurations
	 */
	private ILaunchConfiguration[] getLaunchConfgurations(IResource resource) {
		List<ILaunchConfiguration> configurations = new ArrayList<ILaunchConfiguration>();
		String resourceProjectName = resource.getProject().getName();

		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType type = manager.getLaunchConfigurationType(CleanLaunchConstants.LAUNCH_CONFIG_TYPE_ID);

		try {
			for (ILaunchConfiguration configuration : manager.getLaunchConfigurations(type)) {
				String configurationProjectName = configuration.getAttribute(CleanLaunchConstants.PROJECT_NAME, "");
				if (resourceProjectName.equals(configurationProjectName))
					configurations.add(configuration);
			}
		} catch (CoreException e) {
			System.out.println("could not load launch configuration");
			e.printStackTrace();
		}
		return configurations.toArray(new ILaunchConfiguration[configurations.size()]);
	}

	/**
	 * Is used when the run button is clicked, so that the user doesn't have to manually
	 * make a run configuration. 
	 * Tries to get an existing launch configuration and if there is one or mote, method launches
	 * the first configuration. If not, it makes a new configuration and launches that one.
	 * 
	 * @param file
	 * @param mode
	 */

	private void launchShortcut(final IResource resource, final String mode) {
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().saveAllEditors(true);

		if (resource == null) {
			System.out.println("Error Launching");
			ConsoleInterfacer.writeToConsole(ColorConstants.sRed("Error while launching. Please try again."));
			return;
		}

		try {
			ILaunchConfiguration[] configurations = getLaunchConfgurations(resource);
			if (configurations.length == 0) {
				// If no existing configuration is found, create a new one
				ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
				ILaunchConfigurationType type = manager
						.getLaunchConfigurationType(CleanLaunchConstants.LAUNCH_CONFIG_TYPE_ID);
				IEclipsePreferences preferences = InstanceScope.INSTANCE
						.getNode(ClidePreferenceConstants.PREFERENCE_ID);

				// create new configuration and set attributes
				ILaunchConfigurationWorkingCopy configuration = type.newInstance(null, resource.getName());
				configuration.setAttribute(CleanLaunchConstants.PROJECT_NAME, resource.getProject().getName());
				configuration.setAttribute(CleanLaunchConstants.CPM_LOCATION,
						preferences.get(ClidePreferenceConstants.CPM_LOCATION, ""));
				configuration.doSave();
				configurations = new ILaunchConfiguration[] { configuration };

			}
			configurations[0].launch(mode, new NullProgressMonitor()); // launch the configuration
		} catch (CoreException e) {
			System.out.println("Could not create launch configuration");
		}
	}

}
